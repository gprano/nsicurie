# Interro bases de python

0. Quels sont les opérateurs en python pour :

    * la division arrondie à l'entier inférieur ? **la double barre //**
    * la division qui renvoie un nombre décimal ? **la barre de division simple /**
    * le reste de la division euclidienne ? **le symbole 'modulo' %**

1. Dans l'instruction `y = x + y` :

    * Quelles sont la ou les variables qui doivent déjà exister pour que l'instruction soit valide ? **x et y car l'expression à droite est évaluée avant d'assigner la valeur à y**
    * Quelles sont la ou les variables qui sont créées ou modifiées ? **y**

2. Qu'affiche cette boucle ?
    ```python
    for i in range(3,-1,-1):
        print(i)
    ```
    **de 3 (inclus) à -1 (exclus) par pas de -1, donc:**

    ```
    3
    2
    1
    0
    ```

3. Que peut-il se passer de problématique quand on utilise une boucle while ? 
    
    **Une boucle infinie si la condition est toujours vraie.**

    **Remarque : dans certains cas c'est volontaire, si on veut un programme qui tourne sans terminer (serveur web, jeu, etc)**

4. Quelle est la grossière erreur dans cette définition de fonction ?
   ```python
   def f(100, 200):
       return 100 + 200
   ```
   **Quand on définit la fonction, les paramètres doivent être nommés. C'est seulement quand on appelle la fonction qu'on choisit les valeurs pour les paramètres. Par exemple :**

    ```python
    def f(a, b):
        return a + b
    f(100,200) # s'évalue en 300
    f(-1, 1) # s'évalue en 0
    ```

5. Soit la liste `L = [8,5,6]`. Écrire les instructions pour :

    * Changer le premier élément de la liste en 4 **L[0] = 4**

    &nbsp;

    * Ajouter la valeur 7 à la fin de la liste **L.append(7)**

    &nbsp;


6. Pour une liste stockée dans la variable `L` :

    * Écrire le parcours par valeur pour afficher les éléments de `L`
        ```python
        for v in L:
            print(v)
        ```
        **Remarque : on évite de nommer la variable de boucle i dans ce cas, pour éviter la confusion avec le parcours par indice**
    * Écrire le parcours par indice pour afficher les éléments de `L`
        ```python
        for i in range(len(L)):
            print(L[i])
        ```
    
    
7. Quels sont les deux limitations du parcours par valeur ?
    
    * On ne peut pas **modifier les valeurs de la liste (si on change la variable v, ça ne change pas la liste)**

    * Et on ne peut pas **accéder à l'indice (la position) des valeurs, ce dont on a besoin par exemple si on veut la comparer à la valeur suivante ou précédente, ou tester si c'est la dernière, etc**

8. Quel type peut-on utiliser si dans une fonction on veut renvoyer deux valeurs ? **tuple ou list**
    
    Écrire l'instruction `return` correspondante qui renvoie les valeurs des variables `a` et `b`.

    **avec un tuple : `return a,b` ou `return (a,b)` c'est pareil**

    **avec une liste: `return [a,b]` (moins habituel)**

9. Peut-on utiliser une liste comme :

    * clé dans un dictionnaire ? **Non car les clés d'un dictionnaire doivent être non modifiables, ce qui n'est pas le cas des listes. On peut utiliser des tuples à la place**
    * valeur dans un dictionnaire ? **oui, il n'y a pas de restriction sur les types des valeurs d'un dictionnaire**

10. Pour un dictionnaire stocké dans la variable `d`, écrire la boucle qui affiche toutes les **clés** du dictionnaire.
    ```python
    for cle in d:
        print(cle)
    ```
    **Remarque : encore une fois on évite de nommer la variable de boucle i, qui sert pour parcourir des indices**
