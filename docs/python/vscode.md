# Utilisation de l'éditeur VSCodium

[Aide](https://franckchambon.github.io/ClasseVirtuelle/NSI/5-%C3%89diteurs/vscodium.html) sur le site de F.Chambon, plus complète si vous en avez besoin.

Pour commencer, installez les extensions :

* French Language Pack for Visual Studio Code (recherchez `french` dans le menu des extensions)
* l'extension Python de microsoft (ms-python)
* Pyright, pour tester la vérification statique du typage en python

Ces extensions sont installées à l'intérieur de votre répertoire personnel dans le dossier caché `~/.vscode-oss`,
elles resteront donc disponibles si vous utilisez le même ordinateur au lycée.

## Pour vérifier que tout va bien

Créez un dossier et ouvrez le dans codium.

Par exemple avec les commandes suivantes dans le terminal :

```bash
cd
mkdir test_codium
cd test_codium
codium .
```

!!! info
    La commande `codium REPERTOIRE` ouvre codium avec `REPERTOIRE` comme espace de travail, et `.` est le raccourci dans le terminal pour indiquer la position actuelle.

Créez un nouveau fichier `hello.py`, écrivez le code python qui affiche `hello world !` et exécutez-le.

## Inconvénients du typage dynamique en python

En python, les variables dans le code n'ont pas de type. C'est seulement les **valeurs** que
prennent ces variables à l'exécution qui ont un type, et on aura une erreur si une opération
est faite sur des valeurs qui n'ont pas des types compatibles.

On parle donc de typage **dynamique** (au moment de l'exécution) par opposition aux langages
à typage **statique** qui vérifient le typage avant d'exécuter le programme.

Un désavantage est que dans ce code :

```python

def fonction_presque_jamais_utilisee():
    return 1 + "3"

# etc etc
```

L'erreur commise dans la fonction ne sera pas détectée tant qu'elle n'aura pas été exécutée,
alors qu'on aurait pu le voir facilement en regardant le code.

Ou encore dans ce code là :

```python
def double(x):
    return 2*x

valeur = input()
print(double(valeur))
```

On pourrait vouloir automatiquement détecter que valeur va être de type **str**, et que ça va
poser problème de l'envoyer à la fonction double qui s'attend à un nombre.

## Indications de type en python

Même si ce n'est jamais obligatoire, il est possible d'utiliser d'autres logiciels pour rajouter
une vérification statique du typage en python et détecter certaines erreurs plus rapidement.

Pour cela, python permet d'ajouter des indications de type dans le code, au moment où on
crée les variables et les fonctions.

Pour une variable on ajoute `:` et le type après son nom :
```python
x : int = 3
```

Et pour une fonction on indique le type des paramètres avec `:` et le type de la valeur de
retour avec `->` entre les parenthèses et les deux-points finaux:

```python
def est_positif(x : int) -> bool:
    return (x > 0)
```

!!! info
    * À partir de la version 3.9 de python, il est possible d'utiliser `x : list[int]`
      plutôt que `x : list` pour préciser le type des valeurs contenues dans la liste.
    * Pour un dictionnaire on pourra écrire `d : dict[str,int]` pour indiquer que les clés
      seront de type `str`, et les valeurs de type `int`.
    * À partir de python 3.10, on peut utiliser `|` pour indiquer plusieurs types possibles,
      par exemple `def addition(x : int | float, y : int | float) -> int | float` pour une
      fonction qui prend et renvoie soit des `int` soit des `float`
    * Dans les versions précédentes, on pouvait le faire en utilisant le module `typing`

Par défaut, ce ne sont que des indications que **python ne vérifie pas**.

Mais même sans les vérifier, c'est une bonne pratique de mettre ces indications de type
au moins pour les fonctions. Cela permet en lisant le code de savoir tout de suite les
types attendus en paramètres et renvoyés en valeur de retour.

Certains éditeur de code peuvent afficher ces informations quand on utilise la fonction,
par exemple dans VSCodium :

![type hint in codium](type_hint.png)

On sait immédiatement en tapant `forward` que le paramètre attendu est de type `float`
et que la fonction ne renverra rien (`None`).

## Vérification statique

Python ne fera pas de vérification automatique des indications de type, mais on peut utiliser d'autres logiciels externes pour le faire. Les principaux sont mypy (projet collaboratif indépendant), pytype (développé par google), pycheck (par facebook), et pyright (par microsoft).

On utilisera ici l'extension pyright de codium, mais il en existe aussi une pour mypy.

### exercice 1

Commencez par copier ce code dans codium, et ajoutez les indications de type pour toutes les variables et fonctions :

```python
def triple(x):
    return 3*x

reponse = triple(14)
print(reponse)
```

Vérifiez que vous n'avez pas d'erreur, mais que si vous rajouter la ligne `print(triple("zero"))` pyright vous signale une erreur avant même l'exécution.

### exercice 2

Ajoutez les indications de type pour le paramètre tableau, la valeur de retour de la fonction
et pour les variables locales n et resultat.
Si vous vous trompez de type, l'erreur sera probablement détectée par l'extension pyright.

```python
def sommes_partielles(tableau):
    n = len(tableau)
    resultat = [ 0 for i in range(n)]
    resultat[0] = tableau[0]
    for i in range(1, len(tableau)):
        resultat[i] = resultat[i-1] + tableau[i]
    return resultat

print(sommes_partielles([1,2,3]))
print(sommes_partielles(12)) # cette ligne devrait aussi provoquer une erreur de type
```