# Derniers ajouts

* Sujet centres étrangers groupe 1 [jour 1](https://nodfs.xyz/jour1.pdf) et [jour 2](https://nodfs.xyz/jour2.pdf)

* Rappels de terminal linux avec **gameshell**

    ??? info "Instructions"
        Pour lancer le jeu, ouvrez le terminal et tapez :
        ```bash
        wget https://github.com/phyver/GameShell/releases/download/latest/gameshell.sh
        ```
        Pour télécharger le script gameshell.sh, puis :
        ```bash
        bash gameshell.sh
        ```
        Pour le lancer. Quand vous quittez, cela crée une sauvegarde avec un nom comme `gameshell-save.sh` et il suffit de relancer ce script pour reprendre.

* [Cryptographie](os/tp_cryptographie.md) : expérimentations concrètes

* [Ordonnancement des processus](os/ordonnancement.md) par le système d'exploitation

* [Codex](https://codex.forge.apps.education.fr/) pour s'entraîner à programmer sur des sujets similaires à l'épreuve pratique en ligne

* Calcul d'[itinéraire](structures/itineraire.md) sur un graphe des routes extrait d'openstreetmap.

* [TP](structures/graphes.md) sur les graphes


[Page d'index de l'année précédente](old_index.md)