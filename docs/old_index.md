* Projet de site web + SQL + python :

    * [Aide principale](projets/projet_site.md)
    * [Utilisation de git](projets/git.md) pour le projet.
    * [Mettre en ligne](projets/mise_en_ligne.md) un site créé avec Flask.

* TP sur la [cryptographie](os/tp_cryptographie.md)

* Sujets de la semaine : [Polynésie J1](https://mooc-forums.inria.fr/moocnsi/uploads/short-url/xg3oIRLss3rPqCE2qoeR8121hkC.pdf), [Polynésie J2](https://mooc-forums.inria.fr/moocnsi/uploads/short-url/5hPKx1OHJhtyV7CAuoRRxkopVMv.pdf) et [corrigé](https://mooc-forums.inria.fr/moocnsi/uploads/short-url/ptJSBFqTzPKsxboSTBmL4sL5wxT.pdf), [Centres Étrangers J1](https://mooc-forums.inria.fr/moocnsi/uploads/short-url/g8nsSNT6yIIiBfvjvvNEgKcujmK.pdf) et [Centres Étrangers J2](https://mooc-forums.inria.fr/moocnsi/uploads/short-url/6VGygv0ObZYIDlvAY2Wof6kTQvL.pdf).

* [Sujet et corrigé](corriges/corrige_ordo.md) du devoir sur ordonnancement/terminal/POO.

* Révisions du lundi 6 mars : 

    * exercice écrit sur le routage : [https://e-nsi.forge.aeif.fr/ecrit/2022/metropole-j2/22-ME2-ex3](https://e-nsi.forge.aeif.fr/ecrit/2022/metropole-j2/22-ME2-ex3)
    * exercices pratiques :

        * [sur les listes](https://e-nsi.forge.aeif.fr/pratique/N1/213-liste_differences/sujet/)
        * [sur les dictionnaires](https://e-nsi.forge.aeif.fr/pratique/N1/333-dictionnaire_aime/sujet/)
        * sur capytale (mélange de liste) : `6076-1423224`

* Pour réviser : :new: [morceaux de code essentiels](python/bases.md) en python

* [TP](structures/graphes.md) sur les graphes

* Rappels de terminal linux avec **gameshell**

    ??? info "Instructions"
        Pour lancer le jeu, ouvrez le terminal et tapez :
        ```bash
        wget https://github.com/phyver/GameShell/releases/download/latest/gameshell.sh
        ```
        Pour télécharger le script gameshell.sh, puis :
        ```bash
        bash gameshell.sh
        ```
        Pour le lancer. Quand vous quittez, cela crée une sauvegarde avec un nom comme `gameshell-save.sh` et il suffit de relancer ce script pour reprendre.

* [Utilisation d'API web](seances/api.md)

* [Correction](autres/correction_bacblanc.md) du bac blanc

* Pour info : [Banque des sujets](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2023/) de l'épreuve pratique 2023

* Lundi 30 janvier :
    * Entraînement avec [ce sujet d'écrit](https://e-nsi.forge.aeif.fr/ecrit/2022/metropole-j1/22-ME1-ex3/) sur le réseau.
    * Entraînement à l'épreuve pratique sur e-nsi:

        * combinaison "facile" : [exo1](https://e-nsi.forge.aeif.fr/pratique/N1/115-ind_prem_occ/sujet/) (parcours de liste) et [exo2](https://e-nsi.forge.aeif.fr/pratique/N2/700-poo_chien/sujet/) (programmation orientée objet)
        * combinaison "moyenne" : [exo1](https://e-nsi.forge.aeif.fr/pratique/N1/128-nb_puis_double/sujet/) (boucles imbriquées) et [exo2](https://e-nsi.forge.aeif.fr/pratique/N2/510-unimodal_sommet/sujet/) (variante de dichotomie)
        * combinaison "difficile" : [exo1](https://e-nsi.forge.aeif.fr/pratique/N1/215-exclamations/sujet/) (parcours) et [exo2](https://e-nsi.forge.aeif.fr/pratique/N2/666-percolation/sujet/) (récursivité, grille 2D) ou bien [exo2](https://e-nsi.forge.aeif.fr/pratique/N3/210-rollers/sujet/) (mettre par paires)

* [TP](algo/hufman.md) sur le codage de Huffman (algorithme glouton, binaire, arbres).

* Pour réviser le bac blanc :
    * [Ce qu'il faut savoir sur le binaire et l'hexadécimal](autres/binaire.md) (rappels de 1°)
    * [Cours sur les arbres](https://glassus.github.io/terminale_nsi/T1_Structures_de_donnees/1.3_Arbres/cours/) de G.Lassus
    * [Sujet](bacblanc2022.pdf) du bac blanc de l'an dernier (le dernier exercice de réseau est sur une partie non encore traitée cette année)
        Et :new: [corrigé](https://franckchambon.github.io/ClasseVirtuelle/Term_NSI/devoirs/bac-0/Sujet_zero_20_21.pdf)

* [Ordonnancement des processus](os/ordonnancement.md) par le système d'exploitation
* [DM pour les vacances](dm_serveur_sql.zip) : écrire le SQL d'un serveur web python (forum de discussion) et sa [correction](bdd/corrige_dm.md)

* [TP de rotation d'image](seances/tp_pillow.md) avec une technique "diviser pour régner"
* Bases de données / SQL : 
    * [Cours sur le langage SQL](bdd/sql.md)
    * [Sujet](bdd/exo_sql.pdf) et [corrigé](bdd/corrige_sql.pdf) de l'an dernier
    * [Corrigé](bdd/murder_solution.md) de SQL Murder Mystery (bonus compris)
* [Corrigé](corriges/corrige1.md) du premier devoir de 2h
* [TP](POO/pile_undo.md) pour ajouter le retour en arrière avec Ctrl+Z dans un logiciel.
* [Introduction](python/recursivite1.md) à la récursivité
* [Correction](POO/piles_files.md) des TP sur les piles/files en programmation orienté objet.
* [Tutoriel](https://clogique.fr/nsi/premiere/tuto_pg0/tuto_pgzero.html#1) pour apprendre à utiliser Pygame Zero (pour le 1er projet)
* [Cours](POO/poo.md) sur la Programmation Orientée Objet
* [Pygame Zero : balles rebondissantes 1](POO/balles1.md)
* [VSCodium et typage statique](python/vscode.md)


**Révisions des acquis de 1° NSI :**

* [bases de python](notebooks/python_base.ipynb) et [interro corrigée](python/correction_interro1.md)
* [correction (1/2)](notebooks/dessins_turtle_partie1.ipynb) des dessins avec turtle sur capytale
* [correction (2/2)](notebooks/dessins_turtle_partie2.ipynb) des dessins avec turtle sur capytale
* :stars: [futurecoder](https://futurecoder.forge.aeif.fr) Cours interactif en ligne pour revoir python
* [Rappels de réseau et de web](reseau/reseau_web.md)

