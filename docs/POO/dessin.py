from pgzero.builtins import screen,Rect,keys,keyboard
import pygame

WIDTH = 1000
HEIGHT = 600

PALETTE = ["red", "blue", "green", "gray", "white", "yellow", "orange", "pink", "black"]

class Dessin:
    def __init__(self):
        self.size = (WIDTH-60,HEIGHT)
        self.zone = Rect((0,0), self.size)
        # utiliser la classe Surface de pygame pour l'image
        # plutôt que la dessiner directement sur l'écran
        # permettra de sauvegarder son état plus facilement
        self.image = pygame.Surface(self.size)
        self.image.fill("white")
        self.debut_ligne = None
        self.couleur_ligne = "black"
    def dessine(self):
        screen.blit(self.image,(0,0))
        mouse_pos = pygame.mouse.get_pos()
        # ligne pas encore dessinée (souris non relachée):
        if self.debut_ligne != None and self.zone.collidepoint(mouse_pos):
            screen.draw.line(self.debut_ligne, mouse_pos, self.couleur_ligne)
    def dessine_ligne(self, fin):
        if self.debut_ligne != None:
            pygame.draw.line(self.image, self.couleur_ligne, self.debut_ligne, fin, width=3)
        self.debut_ligne = None
    def change_couleur(self, couleur):
        self.couleur_ligne = couleur

class BoutonCouleur:
    def __init__(self, pos, size, couleur):
        self.zone = Rect(pos, size)
        self.couleur = couleur
    def dessine(self):
        screen.draw.filled_rect(self.zone, self.couleur)

d = Dessin()

lboutons = [BoutonCouleur((WIDTH-50,10+60*i), (40,40), couleur) 
            for i,couleur in enumerate(PALETTE)]

def on_mouse_down(pos):
    if d.zone.collidepoint(pos):
        d.debut_ligne = pos
    for b in lboutons:
        if b.zone.collidepoint(pos):
            d.change_couleur(b.couleur)

def on_mouse_up(pos):
    if d.zone.collidepoint(pos):
        d.dessine_ligne(pos)

def draw():
    d.dessine()
    for b in lboutons:
        b.dessine()

def update():
    pass