# Pygame Zero - Balles rebondissantes 1

**Pygame zero** est une version simplifiée de la bibliothèque Pygame qui sert à créer des jeux en python.

Créez un dossier `balles_rebondissantes` et ouvrez le avec Codium, puis copiez dans un fichier `balles.py` :

```python
import pgzrun
from pgzero.builtins import screen

WIDTH = 640
HEIGHT = 480
COULEUR_BALLE = (255,0,0)
COULEUR_FOND = (0,0,0)

x = 300
y = 200
dx = 4
dy = -3

def draw():
    screen.clear()
    screen.fill(COULEUR_FOND)
    screen.draw.filled_circle((x,y),10,COULEUR_BALLE)

def update():
    global x,y
    x += dx
    y += dy

pgzrun.go()
```

**Exécutez** le code pour observer le résultat (vous pouvez l'arrêter avec Ctrl+C dans le terminal de Codium)

* La bibliothèque est utilisable grâce au `import pgzrun` en haut, et le jeu est lancé avec `pgzrun.go()` à la fin du fichier;
* La bibliothèque appelle alors à intervalles réguliers:
    * `update()` pour mettre à jour les données du jeu
    * `draw()` pour redessiner l'écran
* Les variables créées à l'extérieur d'une fonction ne peuvent pas être ré-assignées dedans. On peut quand même le faire en les déclarant avec `global` au début de la fonction, ce qu'on a fait dans la fonction `update`.

### Rebonds

Faites rebondir la balle lorsqu'elle touche les bords de l'écran.

* Faites cela dans la fonction `update`
* Un rebond sur un des murs revient à inverser la vitesse verticale ou horizontale (dy ou dx) selon le mur
* Réfléchissez au test à faire pour un des murs, et à l'action à prendre. Puis répétez pour les 3 autres murs

### 2 balles

Ajoutez une deuxième balle, avec une vitesse et une position de départ (voire une couleur et une taille) différente.

### 30 balles

Mettez 30 balles en tout. Vous allez devoir changer de stratégie car créer 30 variables à la main pour chaque paramètre serait très pénible. Utilisez des listes.

Générez les positions de départ et les vitesses au hasard
avec la fonction `randint(min, max)` du module `random`. 

### Améliorations

* Utilisez le rayon des balles pour avoir le rebond pile quand le bord de la balle touche le bord de l'écran.

* Si vous avez le temps, essayez de gérer les collisions entre les balles (commencez avec deux balles, et faites un dessin pour comprendre ce qu'il se passe).

??? Info "Collisions"
    * Version simplifiée : si on échange juste les vitesses des deux balles au moment de la collision, cela donne un résultat acceptable.
    * Version réaliste, en utilisant cette formule pour la [collision élastique](https://en.wikipedia.org/wiki/Elastic_collision) :

        ![](formule_collision.png)

        Dans cette formule, les positions **x1** et vitesses **v1** sont des vecteurs à 2 coordonnées, qu'on peut représenter par des tuples.
        Pour calculer ces vitesses, il sera utile de créer des fonctions intermédiaires :
        ```python
        # Pour calculer x1 - x2 sur des vecteurs
        def sub(x1 : tuple, x2 : tuple) -> tuple
        # Pour calculer la norme au carré: x[0]**2 + x[1]**2
        def norme2(x : tuple) -> float
        # Pour calculer le produit scalaire: x1[0]*x2[0] + x1[1]*x2[1]
        def dot(x1 : tuple, x2 : tuple) -> float
        # Pour multiplier un vecteur par une constante
        def prod(c : float, x : tuple) -> tuple
        ```