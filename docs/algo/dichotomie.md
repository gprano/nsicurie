# Recherche par dichotomie (liste **triée**)

## Principe

On a vu par des mesures expérimentales que la recherche d'un élément x dans une liste L avec l'instruction `x in L` en python prend un temps proportionnel à la taille de la liste.

C'est parce que cette instruction est codée comme ceci :

```python
def recherche(L,x):
	for v in L:
		if v == x:
			return True
	return False
```

La boucle `for v in L` est responsable de la complexité linéaire. En fait, on n'a pas le choix : si on veut savoir si un élément est dans une liste, il faut bien regarder tous les éléments.

Mais si la liste est **triée**, on peut utiliser cette information pour aller plus vite.

On compare `x` avec un élément au milieu de la liste :

* si x est égal à cet élément, c'est bon on l'a trouvé.
* si x est plus petit, il ne peut être que dans la moitié gauche de la liste.
* si x est plus grand, il ne peut être que dans la moitié droite de la liste.

Comme on doit ensuite reprendre la recherche de la même manière, on va créer une fonction qui recherche x dans une zone entre l'indice `début` et l'indice `fin` :

## Code

```python
def recherche_dichotomique(L,début,fin,x):
	""" teste si x est dans la liste triée L entre début et fin
	
	paramètres:
		L: une liste triée par ordre croissant
		début: début de la zone de recherche
		fin: fin de la zone de recherche
		x: valeur qu'on cherche
	renvoie:
		un booléen, si x est dans L entre les indices début et fin
	"""
	
	m = (début + fin)//2 #l'indice de l'élément au milieu
	
	if x == L[m]:
		return True
	elif x < L[m]:
		return recherche_dichotomique(L,début,m-1,x)
	else:
		return recherche_dichotomique(L,m+1,fin,x)
```

Ça semble bien, et ça marche quand x est dans la liste, mais si x n'y est pas la fonction ne renvoie rien et on finit par avoir une erreur "maximum recursion depth exceeded" !

En utilisant le mode débug, on se rend compte que la fonction s'appelle elle même infiniment. D'ailleurs, on a nulle part écrit `return False`, donc ça ne pouvait pas marcher !

On voit alors le problème, si on recherche dans une zone d'un seul élément:

* alors `début` est égal à `fin`, donc `m` est égal aux deux.
* donc si x n'est pas égal à L[m], on va chercher entre `début` et `m-1=début-1` ou bien entre `m+1=fin+1` et `fin` : des intervalles qui sont vides car le début est plus grand que la fin !

On peut donc soit détecter cela dans le code :

```python
	elif x < L[m]:
		if début <= m-1:
			recherche_dichotomique(L,début,m-1,fin,x)
		else:
			return False
```

Et on fait la même chose pour l'autre condition.

Ou bien, on peut faire le test au début de l'appel de la fonction :

```python
def recherche_dichotomique(L,début,fin,x):
	if début > fin:
		return False
	#...
```

## Tests

On n'oublie pas d'écrire des tests, si possible avant même d'écrire le code de la fonction :

```python
	assert recherche_dichotomique([1,2,3],0,2,4) == False
	assert recherche_dichotomique([-10,6,20,100,1700],0,4,1700) == True
    #...
```

On peut même tester le code sur des listes aléatoires de grande taille :

```python
	import random
	
	for i in range(100):
		L = [random.randint(-1000,1000) for j in range(1000)]
		L.sort()
		x = random.randint(-1000,1000)
		assert recherche_dichotomique(L,0,len(L)-1,x) == (x in L)
```

