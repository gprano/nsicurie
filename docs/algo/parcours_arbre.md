# Parcours d'arbre

Selon ce qu'on cherche à faire avec une structure de donnée en arbre, on a souvent besoin
de parcourir tous ses éléments (pour en trouver un, en modifier, calculer la taille ou autre...).

Comme ils ne sont pas déjà ordonnés naturellement comme ceux d'une liste, il y a plusieurs manières
de faire.

## Parcours en profondeur

Quand l'arbre est dans une structure récursive, la manière la plus facile d'écrire un
algorithme de parcours est récursive :

```python
def parcourir(arbre):
	if arbre != None:
		parcourir(arbre.gauche)
		parcourir(arbre.droit)
```

L'appel `parcourir(arbre.gauche)` recommence l'exécution de la fonction sur le sous-arbre gauche,
et peut elle même faire d'autres appels si ce sous-arbre n'est pas vide (`None` dans notre code).
On va donc parcourir tout le sous-arbre gauche avant de revenir et faire l'appel pour le sous-arbre
droit, et cela à chaque noeud.

L'ordre de parcours des noeuds est donc le suivant:
```mermaid
graph TD
A((1)) --- B((2))
A --- C((6))
B --- D((3))
B --- E((4))
E --- G((5))
C --- F((7))
```

Si on affiche la valeur de chaque noeud au passage :

```python
def parcourir(arbre):
	if arbre != None:
		print(arbre.valeur)
		parcourir(arbre.gauche)
		parcourir(arbre.droit)
```

Alors les valeurs seront bien affichées dans l'ordre donné par le graphique, c'est
un parcours _préfixe_ (car on affiche la valeur avant les appels récursifs).

Mais on peut aussi changer la place du print, et afficher la valeur de l'arbre entre le
parcours du sous-arbre gauche et celui du sous-arbre droit :

```python
def parcourir(arbre):
	if arbre != None:
		parcourir(arbre.gauche)
		print(arbre.valeur)
		parcourir(arbre.droit)
```
C'est un parcours _infixe_, et l'ordre sera le suivant :

```mermaid
graph TD
A((1)) --- B((2))
A --- C((6))
B --- D((3))
B --- E((4))
E --- G((5))
C --- F((7))
```

```dot
digraph G {
    nodesep=0.4; //was 0.8
    ranksep=0.5;

    {node[style=invis,label=""]; cx_30;
    }
    {node[style=invis, label="", width=.1]; ocx_45; ocx_20;
    }

    {rank=same; 20; 45; cx_30}
    {rank=same; 10; 25; ocx_20}
    {rank=same; 40; 50; ocx_45}

    30 -> 20;
    30 -> 45;
    20 -> 10;
    20 -> 25;

    45 -> 40;
    45 -> 50;

    {edge[style=invis];
                        //Distantiate nodes
                        30 -> cx_30;
                            20 -> cx_30 -> 45;

                        //Force ordering between children
                        45 -> ocx_45;
                            40 -> ocx_45 -> 50;
                        20 -> ocx_20;
                            10 -> ocx_20 -> 25;
    } 
} 
```


## Parcours en largeur

```mermaid
graph TD
A((1)) --- B((2))
A --- C((3))
B --- D((4))
B --- E((5))
E --- G((7))
C --- F((6))
```

### Principe

### En python

