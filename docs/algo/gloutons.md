# Les Algorithmes Gloutons

Un **algorithme glouton** (_greedy_ en anglais) est un type d'algorithme pour résoudre des problèmes
d'optimisation : parmi de nombreuses solutions possibles, trouver celle qui est la meilleure
selon un certain critère.

La stratégie gloutonne consiste à faire le meilleur choix à chaque petite étape, en espérant que
la solution obtenue soit la meilleure à la fin.

Une autre stratégie pour les problèmes d'optimisation est la **force brute** (_bruteforce_ en anglais) :
tester toutes les possibilités, et garder la meilleure.

La stratégie par force brute trouve toujours la meilleure solution, mais elle peut être lente ou même
complétement impraticable si le nombre de possibilités est trop grand.

Bien souvent, la stratégie gloutonne ne donne _pas_ le résultat optimal, mais elle est quand même utile
car elle est facile à coder et rapide d'exécution. Un algorithme qui donne une solution approchée est appelé 
une **heuristique**.

## Exemples de problèmes d'application

| Nom | Objectif | Stratégie gloutonne | Optimalité du glouton |
|-----|----------|-----------------------|----|
|Rendu de monnaire|Rendre une somme avec le moins de pièces/billets.|On rend à chaque fois la plus grande pièce/billet possible.|Seulement si le système monétaire est "canonique".<br>C'est le cas avec [200,100,50,20,10,5,2,1].<br>Mais pas avec [10,6,1].|
|Sac à dos|Le sac a une capacité en poids limitée, chaque objet a un poids et une valeur, il faut trouver l'ensemble d'objets qui rentre dans le sac avec une valeur totale maximale.|On prend à chaque fois l'objet qui rentre avec la plus grande valeur, ou le plus petit poids, ou le meilleur ratio valeur/poids.|Non, aucune des trois stratégies gloutonnes ne donne toujours le meilleur résultat possible.|
|Voyageur de Commerce|On doit visiter une liste de ville à partir d'une ville de départ puis y revenir. Quel est le chemin le plus court pour le faire ?|On va à chaque fois à la ville la plus proche pas encore visitée.|Non.

