# Algorithmes de tri

## Tri par sélection O(n²)

Principe : on place les éléments directement au bon endroit, du plus petit au plus grand.
À chaque étape on **sélectionne** le minimum dans les éléments restants, et on le place au début
des éléments restants.

On a besoin de savoir trouver la position du minimum dans la liste à partir d'une certaine
position, et de savoir échanger les éléments à deux positions données de la liste, ce qu'on
fait dans des fonctions à part. Cela permet de découper le code en trois parties plus simples.

```python
def position_minimum(l, depart):
    """renvoie la position du minimum dans la liste l à partir de la position depart"""
    n = len(l)
    imin = depart
    for i in range(depart,n):
        if l[i] < l[imin]:
            imin = i
    return imin
```

```python
def echange(l,a,b):
    """échange les éléments en positions a et b dans la liste l"""
    sauvegarde_a = l[a]
    l[a] = l[b]
    l[b] = sauvegarde_a
```

```python
def tri_selection(l):
    """trie la liste l (la liste est modifiée en place, cette fonction ne renvoie rien)"""
    n = len(l)
    for i in range(n):
        imin = position_minimum(l, i)
        echange(l, imin, i)
```


## Tri par insertion O(n²)

Principe : On prend chaque élément de la liste, et on l'**insère** à sa place
dans la partie à sa gauche qui est déjà rangée dans l'ordre (ce ne sont pas
forcément les plus petits éléments comme dans le tri par sélection, mais ils
sont bien ordonnés). Pour le mettre à sa place, on l'échange avec son voisin de
gauche tant que celui-ci est plus grand et qu'on n'est pas arrivé tout à gauche.

```python
def tri_insertion(l):
    """trie la liste l (la liste est modifiée en place, cette fonction ne renvoie rien)"""
    n = len(l)
    for i in range(n):
        j = i
        while j>0 and l[j-1]>l[j]:
            echange(l,j,j-1)
            j -= 1
```

## Tri fusion O(n.log(n))

Principe: 

* Si la liste à trier a moins de deux éléments, on ne fait rien.
* Sinon, on la coupe en deux parties, qu'on trie chacune récursivement avec la même méthode.
* Puis on les fusionne. On peut ajouter les éléments dans l'ordre dans la liste fusionnée
en comparant seulement à chaque fois les premiers éléments de chaque partie. En effet le
plus petit élément est forcément le plus petit de l'une des parties.

```python
def tri_fusion(l):
    n = len(l)
    if n<=1:
        return l
    # Diviser (rapide avec les slices)
    l1,l2 = l[:n//2],l[n//2:]
    # Trier récursivement les parties
    l1 = tri_fusion(l1)
    l2 = tri_fusion(l2)
    # Fusion
    i1,i2 = 0,0
    res = []
    while i1<len(l1) and i2<len(l2):
        if l1[i1]<l2[i2]:
            res.append(l1[i1])
            i1+=1
        else:
            res.append(l2[i2])
            i2+=1
    # On ajoute la fin des deux listes (avec les slices encore)
    # Si i1==len(l1), alors l1[i1:] est une liste vide donc pas d'erreur
    res.extend(l1[i1:])
    res.extend(l2[i2:])
    return res
```
