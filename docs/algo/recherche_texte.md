
# Algorithmique du texte

L'algorithmique du texte est le domaine de l'algorithmique qui traite des 
chaînes de caractères. Les problèmes qu'on se pose sont la recherche d'un motif
dans un texte, la détection de textes similaires, l'indexation des données, etc.

Ces algorithmes peuvent ensuite être appliqués pour le langage naturel (correction
automatique, autocomplétion, détection de plagiat), d'autres domaines scientifiques
(analyse des séquences d'ADN par exemple), ou simplement dans des outils informatiques
(recherche dans une base de donnée, un dépôt de code informatique, ...).

# Algorithmes de recherche de sous-chaîne

Le but ici est de chercher une sous-chaîne ou motif dans un texte, c'est ce qu'il se
passe quand on utilise la fonction de recherche de la plupart des logiciels (Ctrl+F).

On notera **n** la taille du texte, **m** la taille du motif.

## Algorithme naïf

On essaye tous les alignements du motif avec le texte possible, et à chaque fois
on compare les lettres du motif avec celle du texte de gauche à droite jusqu'à
trouver deux lettres différentes ou jusqu'à arriver à la fin du motif: dans ce cas,
le motif est présent dans le texte à cet endroit.

```
WIKIPEDIA
KIWI
 KIWI
  KIWI
   KIWI
    KIWI
     KIWI
```

La complexité est en:

* $O(n)$ dans le meilleur cas : si la première lettre est toujours
différente.
* $O(n*m)$ dans le pire cas: si on doit tester le motif jusqu'au bout à chaque fois.

??? "Code de l'algorithme naïf"
    ```python
    def nb_apparitions_naif(texte : str, motif : str) -> int:
        n = len(texte)
        m = len(motif)
        resultat = 0
        for depart in range(n):
            if depart + m - 1 < n:
                i_motif = 0
                while i_motif < m and texte[depart + i_motif] == motif[i_motif]:
                    i_motif += 1
                if i_motif == m:
                    resultat += 1
        return resultat
    ```

## Algorithme de Boyer-Moore-Horspool

Cet algorithme reprend l'algorithme naïf mais compare à chaque fois le motif **à
l'envers, à partir de la dernière lettre**.

L'intérêt est qu'une fois la comparaison terminée (occurence du motif ou première
erreur rencontrée), on peut faire avancer le motif jusqu'à réaligner la lettre de fin
avec sa dernière apparition dans le motif.

<pre>
WI<b style="color:red;">K</b><b style="color:green;">I</b>P<b style="color:red;">E</b>DIA
KI<b style="color:red;">W</b><b style="color:green;">I</b>
&nbsp;&nbsp;KIW<b style="color:red;">I</b>          #décalage de 2 pour aligner le I
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;KIW<s style="color:red;">I</s>      #décalage de 4 car E n'est pas dans KIWI
</pre>

On peut donc précalculer pour le motif la table des décalages en fonction de la
lettre lue dans le texte. Pour "KIWI" cela donne:


|lettre|décalage|
|------|------|
|K|3|
|I|2|
|W|1|
|autre|4|

La complexité obtenue est en:

* $O(n / m)$ dans le meilleur des cas, si on décale de la longueur du motif à chaque fois.
* $O(n * m)$ dans le pire cas.

Comme pour l'algorithme naïf, le pire cas est par exemple obtenu quand le texte n'est composé que de la lettre "A" et le motif aussi : la table de décalage ne permet pas de décaler de plus de 1 à la fois et il faut faire toutes les comparaisons.

??? "Code de Boyer-Moore-Horspool"
    ```python
    def table_decalage(motif : str):
        m = len(motif)
        derniere_position = {}
        # /!\ si on met range(m) la dernière lettre a un décalage de 0 => boucle infinie
        for i in range(m-1):
            derniere_position[motif[i]] = i
        decalage = {}
        for lettre in derniere_position:
            decalage[lettre] = m - 1 - derniere_position[lettre]
        return decalage

    def nb_apparitions_bm(texte : str, motif : str) -> int:
        n = len(texte)
        m = len(motif)
        resultat = 0
        decalage = table_decalage(motif)
        depart = 0
        while depart + m - 1 < n:
            i_motif = m - 1 # on commence par la dernière position du motif
            while i_motif >= 0 and texte[depart + i_motif] == motif[i_motif]:
                i_motif -= 1
            if i_motif < 0: # match complet
                resultat += 1
            derniere_lettre = texte[depart + m - 1]
            if derniere_lettre in decalage:
                depart += decalage[derniere_lettre]
            else:
                depart += m
        return resultat
    ```

Voici aussi une version un paramètre booléen en plus pour les fonctions de recherche qui permet d'afficher toutes les étapes si on le met à `True`:

??? "Code avec affichage"
    ```python
    def print_compare(texte, itexte, motif, imotif,decalage):
        def print_highlight(s,i_hi,color):
            for i in range(len(s)):
                if i==i_hi:
                    print(f"\x1b[1;{color}m{s[i]}\x1b[22;0m",end="")
                else:
                    print(s[i],end="")
            print()
        color = 32 if texte[itexte]==motif[imotif] else 31
        print_highlight(texte,itexte,color)
        print(" "*decalage,end="")
        print_highlight(motif,imotif,color)
            
    def nb_apparitions_naif(texte : str, motif : str, verbose : bool) -> int:
        n = len(texte)
        m = len(motif)
        resultat = 0
        for depart in range(n):
            if depart + m - 1 < n:
                i_motif = 0
                while i_motif < m:
                    if verbose:
                        print_compare(texte,depart+i_motif,motif,i_motif,depart)
                    if texte[depart + i_motif] != motif[i_motif]:
                        break
                    i_motif += 1
                if i_motif == m:
                    resultat += 1
        return resultat

    def table_decalage(motif : str):
        m = len(motif)
        derniere_position = {}
        for i in range(m-1): # m-1 sinon bug, décalage de 0 pour la dernière lettre => boucle infinie
            derniere_position[motif[i]] = i
        decalage = {}
        for lettre in derniere_position:
            decalage[lettre] = m - 1 - derniere_position[lettre]
        return decalage

    def nb_apparitions_bm(texte : str, motif : str, verbose : bool) -> int:
        n = len(texte)
        m = len(motif)
        resultat = 0
        decalage = table_decalage(motif)
        depart = 0
        while depart + m - 1 < n:
            i_motif = m - 1 # on commence par la dernière position du motif
            while i_motif >= 0:
                if verbose:
                    print_compare(texte,depart+i_motif,motif,i_motif,depart)
                if texte[depart + i_motif] != motif[i_motif]:
                    break
                i_motif -= 1
            if i_motif == -1: # match complet
                if verbose:
                    print("~~ Match ! ~~")
                resultat += 1
            derniere_lettre = texte[depart + m - 1]
            if verbose:
                print(f"decalage de {decalage.get(derniere_lettre,m)} (dernière lettre : {derniere_lettre})")
            if derniere_lettre in decalage:
                depart += decalage[derniere_lettre]
            else:
                depart += m
        return resultat

    print(nb_apparitions_naif("WIKIPEDIAKIWIKIWITYYGHOEGIZUHCU","KIWITYYGH",True))
    ```
