# Parcours en profondeur et en largeur de graphes

## Graphes utilisés

On travaille avec `graphe1.txt` qui contient une représentation de ce
graphe simple pour tester facilement le code :

```graphviz dot graphe1.svg
graph G {
    A -- B;
	A -- G;
	B -- C;
	B -- D;
	C -- D;
	D -- F;
	D -- E;
	G -- F;
}
```

Et avec `graphe2.txt` qui contient le graphe des mots valides au scrabble
de 5 lettres. Deux mots sont reliés par une arête si on peut passer de l'un
à l'autre en changeant une lettre (7645 sommets, 24350 arêtes). Le but du "jeu"
associé est de passer entre deux mots donnés par une suite de mots intermédiaires
qui ne changent à chaque fois que d'une lettre.

## Parcours en profondeur

Le graphe en python est représenté par un dictionnaire qui associe à chaque
sommet la liste de ses voisins (on parle de **listes d'adjacence**).
Cette représentation permet d'accéder facilement aux voisins d'un sommet,
elle est donc pratique pour parcourir le graphe.

Si on procède comme pour le parcours en profondeur des arbres on écrirait le code:

```python
def parcours(sommet):
    print(sommet)
    for voisin in g[sommet]:
        parcours(voisin)
```

Mais on obtient des appels récursifs infinis qui affichent A,B,A,B,A...

!!! tip "Important"
	Dans un graphe, il peut y avoir des boucles et on doit donc
	retenir les sommets déjà parcourus pour éviter de les reparcourir.
	
On peut faire cela avec un dictionnaire ou une liste, mais un dictionnaire
sera plus efficace : `sommet in dejavu` s'exécute en temps constant quelle que
soit la taille du dictionnaire, alors qu'une liste serait parcourue en entier.
Ce dictionnaire ne sert qu'à retenir un ensemble de clés, les sommets déjà vus,
donc la valeur associée n'est pas importante (on prend 42).

```python
dejavu = {}
def parcours(sommet):
    if sommet in dejavu:
        return
	dejavu[sommet] = 42
    print(sommet)
    for voisin in g[sommet]:
        parcours(voisin)
```

On obtient alors :

```python
>>> parcours("A")
A B C D F G E 
```

En rajoutant un test, le parcours en profondeur permet de savoir si
un sommet est accessible depuis un autre, par exemple sur le graphe 2 :

??? tldr "Parcours pour accessibilité"
	```python
	dejavu = {}
    def parcours(sommet):
        if sommet in dejavu:
            return
        dejavu[sommet] = 42
        if sommet == "RAYON":
            print("accessible !")
        for voisin in g[sommet]:
            parcours(voisin)
    ```
    Ce qui donne :
    ```
    >>> parcours("RAYON")
    accessible !
    >>> parcours("FUTON")
    >>>
    ```

On peut améliorer le code pour mettre le sommet de destination en paramètre,
et renvoyer True/False plutôt qu'un affichage avec print:

??? tldr "Parcours pour accessibilité amélioré"
    ```python
    dejavu = {}
    def parcours(sommet : str, destination : str) -> bool:
        if sommet in dejavu:
            return False
        dejavu[sommet] = 42
        if sommet == destination:
            return True
        for voisin in g[sommet]:
            trouvé = parcours(voisin,destination)
            if trouvé:
                return True
        return False
    ```
    Ce qui permet :
    ```python
    >>> parcours("RAYON","FUTON")
    False
    >>> parcours("RAYON","AGILE")
    True
    ```

On peut aussi :

??? tldr "Rajouter un paramètre pour obtenir la distance dans le parcours"
    On renvoie la distance, en utilisant -1 si on n'a pas trouvé la destination.
    ```python
    dejavu = {}
    def parcours(sommet : str, destination : str, distance : int) -> bool:
        if sommet in dejavu:
            return -1
        dejavu[sommet] = 42
        if sommet == destination:
            return distance
        for voisin in g[sommet]:
            resultat = parcours(voisin,destination,distance+1)
            if resultat >= 0:
                return resultat
        return -1
    ```
    On obtient :
    ```python
    >>> parcours("RAYON","AGILE",0)
    1926
    ```
    Remarque : ce n'est pas la distance la plus courte possible !

??? tldr "Rajouter un paramètre pour obtenir le chemin parcouru"
    On renvoie le chemin, avec la liste vide si on n'a pas trouvé la destination.
    ```python
    dejavu = {}
    def parcours(sommet : str, destination : str, chemin : list) -> bool:
        if sommet in dejavu:
            return []
        dejavu[sommet] = 42
        if sommet == destination:
            return chemin
        for voisin in g[sommet]:
            resultat = parcours(voisin, destination, chemin + [sommet])
            if resultat != []:
                return resultat
        return []
    ```
    On obtient :
    ```python
    >>> parcours("RAYON","AGILE",[])
    ['RAYON', 'CAYON', 'CAION', 'CANON',...
    ```

## Utilisation : composantes connexes

Dans un graphe non-orienté, si deux sommets ne sont pas reliables
par un chemin, c'est qu'il sont dans deux parties du graphes entre
lesquelles il n'y a aucune arête. On appelles ces parties du graphes
des composantes connexes.

Si un graphe est connexe, il n'a qu'une composante connexe, sinon il
en a plusieurs. Le graphe suivant a trois composantes connexes :

```graphviz dot graphe1.svg
graph G {
    A -- B;
	A -- C;
    B -- C;
    E -- F;
    G;
}
```

Pour mieux comprendre la structure du graphe 2 (Est-ce que la plupart des
mots sont accessibles de cette façon ? Si je prends deux mots au hasard,
est-ce probable qu'il soient reliables par un chemin ?), on peut vouloir trouver
ses composantes connexes.

On peut marquer les composantes connexes à l'aide de parcours en profondeur,
puis compter leur nombre et leur taille.

[À FINIR]

## Parcours en largeur

Si on reprend le parcours en profondeur de base, on peut afficher le sommet
avant et après les appels récursifs. On obtient :

??? tldr "Affichage de début et fin d'appel"
    ```python hl_lines="6 9"
    dejavu = {}
    def parcours(sommet : str) -> None:
        if sommet in dejavu:
            return []
        dejavu[sommet] = 42
        print("Ajout",sommet)
        for voisin in g[sommet]:
            parcours(voisin)
        print("Fin",sommet)
    ```
    On obtient:
    ```python
    >>> parcours("A")
    Ajout A
    Ajout B
    Ajout C
    Ajout D
    Ajout F
    Ajout G
    Fin G
    Fin F
    Ajout E
    Fin E
    Fin D
    Fin C
    Fin B
    Fin A
    ```
    
On observe que l'ordre de opérations est celui d'une **pile** : le sommet qui se
termine est à chaque fois le dernier ajouté. Cette observation permet de 
recoder le parcours en profondeur sans appel récursif,
en utilisant vraiment une pile.

??? tldr "Parcours en profondeur itératif (= non-récursif)"
    On utilise une liste python comme pile (ajout avec _append_ et retrait
    avec _pop_), c'est efficace.
    ```python
    def parcours_iteratif(sommet_depart):
        pile = [sommet_depart]
        dejavus = {}
        while len(pile)>0:
            sommet = pile.pop()
            if sommet not in dejavus:
                dejavus[sommet] = 42
                print(sommet)
                for voisin in g[sommet]:
                    pile.append(voisin)
    ```
    On obtient aussi un parcours en profondeur.
    
Que se passe-t-il si on reprend ce code mais qu'on remplace la pile par une
**file** ?

* Avec une pile, après avoir visité un sommet on visite toujours un de ses
voisins, parce que c'est ceux qu'on vient de mettre en haut de la pile.
* Avec une file, on commence par visiter tous les voisins du
sommet de départ, car les sommets sont visités dans l'ordre où ils ont été
ajoutés. Puis seront ajoutés tous les voisins de ces sommets là, c'est à dire
les sommets à distance 2 du sommet de départ, qu'on va visiter, et ainsi de suite...

Donc comme pour les arbres on obtient le parcours en **largeur**, qui visite les
sommets par ordre de distance croissante au sommet de départ.

??? tldr "Parcours en largeur"
    ```python
    def parcours_largeur(sommet_depart):
        from queue import Queue
        file = Queue()
        file.put(sommet_depart)
        dejavus = {}
        while not file.empty():
            sommet = file.get()
            if sommet not in dejavus:
                dejavus[sommet] = 42
                print(sommet)
                for voisin in g[sommet]:
                    file.put(voisin)
    ```
    On obtient:
    ```
    >>> parcours_largeur("A")
    A
    B
    G
    C
    D
    F
    E
    ```

On peut utiliser le parcours en largeur pour trouver la **distance la plus
courte** entre deux sommets, en ne mettant dans la file pas seulement le sommet
mais la paire (sommet, distance). Le sommet de départ étant à distance 0.

??? tldr "Distance la plus courte"
    ```python hl_lines="4 7 13"
    def parcours_largeur(sommet_depart,destination):
        from queue import Queue
        file = Queue()
        file.put((sommet_depart,0))
        dejavus = {}
        while not file.empty():
            sommet,distance = file.get()
            if sommet not in dejavus:
                dejavus[sommet] = 42
                if sommet == destination:
                    print("trouvé à distance",distance)
                for voisin in g[sommet]:
                    file.put((voisin,distance+1))
    ```
    On obtient sur le graphe 2:
    ```python
    >>> parcours_largeur("AGILE","RAYON")
    trouvé à distance 13
    ```
    
On peut aussi mettre dans la file le tuple (sommet, chemin) avec chemin la liste
des sommets parcourus pour arriver à ce sommet, afin d'obtenir la solution
la plus courte au jeu des mots :

??? tldr "Chemin le plus court"
    L'expression `chemin + [sommet]` crée une nouvelle liste en ajoutant
    sommet à la fin de la liste chemin.
    ```python hl_lines="4 7 13"
    def parcours_largeur(sommet_depart,destination):
        from queue import Queue
        file = Queue()
        file.put((sommet_depart,[]))
        dejavus = {}
        while not file.empty():
            sommet,chemin = file.get()
            if sommet not in dejavus:
                dejavus[sommet] = 42
                if sommet == destination:
                    print("trouvé avec le chemin",chemin)
                for voisin in g[sommet]:
                    file.put((voisin,chemin + [sommet]))
    ```
    On obtient sur le graphe 2:
    ```python
    >>> parcours_largeur("AGILE","RAYON")
    trouvé avec le chemin ['AGILE', 'AGITE', 'ALITE', 'ALISE', 'ARISE', 'BRISE', 'BAISE', 'BAINE', 'BANNE', 'CANNE', 'CANOE', 'CANON', 'CAYON', 'RAYON']
    ```
