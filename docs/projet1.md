# Jeu en Programmation Orientée Objet (avec Pygame Zero)

Durée : environ 4 séances en classe jusque après les vacances de la Toussaint

Objectif : réaliser un petit jeu avec Pygame Zero en créant des classes pour les éléments du jeu.

Ressources :

* Ce qu'on a fait sur les [balles rebondissantes](POO/balles1.md).
* Le [TP](https://clogique.fr/nsi/premiere/tuto_pg0/tuto_pgzero.html#1) d'introduction à pygame zero avec l'alien.
* La [documentation](https://pgzero-french.readthedocs.io/fr/latest/) en français de Pygame Zero.
* Vous pouvez partagez des fichiers à [ce lien](https://nuage03.apps.education.fr/index.php/s/k35aBGxdTzFXDgP) (protégé par mot de passe)
* :new: [Page d'aide](aide_projet1.md) avec des informations rajoutées au fur et à mesure

## Première étape

Réfléchir, écrire et m'envoyer sur l'ENT un document texte qui contienne :

1. la description précise du fonctionnement du jeu dans sa version minimale (y compris les touches/bouton pressées, et tous les événements qu'on peut avoir dans le jeu).

2. les extensions que vous aimeriez ajouter si vous avez le temps.

3. Les classes que vous pensez créer, et pour chaque classe les méthodes que vous pensez créer (une méthode : une action faite sur l'objet, déplacement par exemple).

4. Les problèmes que vous anticipez (ce que vous n'avez pas d'idée de comment vous allez pouvoir le faire par exemple).

5. Les étapes à faire dans l'ordre pour arriver au résultat décrit en 1.

Mettre dans le sujet du message les prénoms des élèves de votre groupe.

## Deuxième étape

Écrivez une version de votre code avec :

* toutes les classes dont vous aurez besoin
* les attributs de la classe dans la méthode `__init__`
* pour les autres méthodes seulement les paramètres et une docstring qui indique ce qu'elle fera
* vous pouvez utiliser `...` pour ce qu'il y aura à compléter, ainsi votre code ne provoquera pas d'erreurs (c'est une valeur spéciale en python)

Par exemple si vous créez une classe Balle vous pourrez avoir :

```python
class Balle:
    def __init__(self, x, y):
        """la position est donnée en paramètre,
        la vitesse est générée au hasard"""
        self.x = ...
        self.y = ...
        self.vitesse = ...
    def dessine(self):
        """dessine la balle sur l'écran"""
        ...
    def bouge(self):
        """bouge la balle selon sa vitesse
        gère aussi les rebonds sur les murs"""
        ...
```

Puis créez les objets nécessaires, et faites une version simple des fonctions `draw` et `update` qui utilise ces méthodes même si elles ne sont pas encore codées, par exemple :

```python
laby = Labyrinthe()
balle = Balle(100,100)

def draw():
    laby.dessine()
    balle.dessine()

def update():
    balle.bouge()
    if balle.est_arrivee():
        print("Gagné")
        exit(0)
```

Votre code devrait fonctionner sans erreurs même si il ne fait pas encore tout ce que vous voulez (car certaines méthodes sont vides).

## Rendu final

Vous devrez rendre, dans votre dossier sur le nextcloud :

* le code du projet et tout ce dont il a besoin pour fonctionner (images etc)
* un fichier `README.txt` qui précise :
    * les problèmes éventuels qu'il reste à la fin, ce que vous avez fait pour essayer de les régler, pourquoi ça n'a pas marché. Ceci est important et servira pour l'évaluation : même s'il n'a pas abouti, le travail que vous avez fait est formateur et valorisable.
    * ce que vous avez utilisé comme aide. S'il y a du code dans le projet qui n'est pas de vous, il faut absolument préciser quelle partie et mettre les liens vers là où vous l'avez pris (je sanctionnerai si le code n'est pas de vous et que vous ne l'avez pas précisé).

Il y aura une présentation orale rapide à la classe dont le but est :

* de montrer ce que vous avez fait, en faire la démo si c'est fonctionnel
* d'expliquer un aspect qui peut être intéressant pour les autres : comment vous avez résolu un problème qui s'est posé à vous, une idée dans le code, ...




























