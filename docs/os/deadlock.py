from threading import Thread,Lock

verrouA = Lock()
verrouB = Lock()

def programme1():
    for i in range(1000):
        verrouA.acquire()
        verrouB.acquire()
        print("truc important du thread 1 qui a besoin des verrous A et B")
        verrouB.release()
        verrouA.release()

def programme2():
    for i in range(1000):
        verrouB.acquire()
        verrouA.acquire()
        print("truc important du thread 2 qui a besoin des verrous A et B")
        verrouB.release()
        verrouA.release()

    
# Création des threads
t1 = Thread(target=programme1)
t2 = Thread(target=programme2)

# Lancement des threads
t1.start()
t2.start()

# Attente de la fin d'exécution
t1.join()
t2.join()

print("C'est fini!")