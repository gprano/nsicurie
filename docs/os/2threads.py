# Librairie utilisées
from threading import Thread 



def fonctionA(limite):
    for i in range(0, limite):
        print("A",end="")

def fonctionB(limite):
    for i in range(0, limite): 
        print("B",end="")


if __name__ == '__main__':
    # Création des threads
    n = 1000000
    t1 = Thread(target=fonctionA, args=(n,))
    t2 = Thread(target=fonctionB, args=(n,))

    # Lancement des threads
    t1.start()
    t2.start()

    # Attente de la fin du travail
    t1.join()
    t2.join()

