# Librairie utilisées
from threading import Thread 

# Variable partagée par les 2 threads
compteur = 0

def incrementer(limite):
    """Incrémente le compteur"""
    global compteur
    for i in range(0, limite):
        compteur = compteur + 1


def decrementer(limite):
    """Décrémente le compteur"""
    global compteur
    for i in range(0, limite): 
        compteur = compteur - 1


if __name__ == '__main__':
    print(f"Compteur au début => {compteur}")
    n = 10**6
    # Création des threads
    t1 = Thread(target=incrementer, args=(n,))
    t2 = Thread(target=decrementer, args=(n,))

    # Lancement des threads
    t1.start()
    t2.start()

    # Attente de la fin du travail
    t1.join()
    t2.join()

    # Affichage du résultat
    print(f"Compteur à la fin => {compteur}")
