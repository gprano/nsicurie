# Rappels de binaire/hexadécimal

## Définitions

Les nombres sont les mêmes mais on peut les _représenter_ dans différentes bases :

* en base 10 (système décimal), on écrit 789 parce que c'est $7\times 10^2 + 8\times 10^1 + 9(\times 10^0)$;
* en base 2 (binaire), 789 s'écrit $1100010101_b$
car il est égal à $2^9 + 2^8 + 2^4 + 2^2 + 2^0$
* en base 16 (hexadécimal), 789 s'écrit $315_{h}$ car il est égal à $3\times 16^2 + 1\times 16 + 5$. Pour les chiffres représentant 10 à 15 on utilise les lettres de a à f.

Ce sont les bases les plus courantes utilisées en informatique. Le binaire parce que c'est ce qui est utilisé par l'ordinateur, et l'hexadécimal parce que $16=2^4$ donc la conversion est facile : un chiffre hexadécimal correspond à 4 chiffres binaires. C'est donc une manière plus courte et lisible d'écrire du binaire, utilisée par exemple pour les couleurs en CSS ou les adresses IPv6.

!!! info "Notations"

    Pour ne pas confondre 101 en binaire (qui vaut 5) avec 101 en base 10 on peut écrire $101_b$.

    En python on préfixe par `0b` pour écrire un nombre en binaire et par `0x` pour écrire un nombre en hexadécimal. Ainsi `0b1100010101` et `0x315` valent tous les deux `789`, de type int.

## Conversions

### Binaire vers base 10

* À la main il suffit d'additionner les puissances de 2 correspondantes : $101_b = 2^2 + 2^0 = 4 + 1 = 5$
* En python il suffit d'écrire dans la console :
    ```python
    >>> 0b101
    5
    ```
    Car python affiche les `int` en base 10 par défaut.

### Base 10 vers binaire

* À la main on peut soit :

    * Enlever successivement les plus grandes puissances de 2 possibles jusqu'à arriver à zéro. Par exemple pour 42 on enlève 32 (reste 10) puis 8 (reste 2) puis 2. On a donc
    $42 = 2^5 + 2^3 + 2^1 = 101010_b$
    * Ou bien on peut utiliser l'algorithme suivant pour trouver les chiffres de droite à gauche :

        **tant que** le nombre n'est pas zéro:
        
        * écrire 0 si le nombre est pair, 1 sinon
        * diviser le nombre par 2 en arrondissant en dessous

        C'est la méthode la plus simple à programmer :

        ```python
        def representation_binaire(n):
            resultat = ""
            while n > 0:
                if n % 2 == 0:
                    # on rajoute les chiffres à gauche
                    resultat = "0" + resultat 
                else:
                    resultat = "1" + resultat
                n = n // 2
            return resultat
        ```
* En python on peut utiliser la fonction `bin` (et `hex` pour l'hexadécimal) qui renvoient des chaînes de caractères :

    ```python
    >>> bin(789)
    '0b1100010101'
    >>> hex(789)
    '0x315'
    ```

### Binaire <-> Hexadécimal

Il suffit de convertir par bloc de 4 chiffres :

$101101_b$ a les blocs (en complétant à gauche par des 0) $0010$ et $1101$ qui valent $2$ et $13=d$ donc ce nombre s'écrit $2d_h$ en hexadécimal.

Dans l'autre sens si on a $f5_h$ en hexadécimal on peut convertir $f=15=1111_b$ et $5=0101_b$ donc $f5_h=11110101_b$.

## Diverses choses à savoir

* Les nombres représentés en binaire par 1 suivis de zéros sont les puissances de 2. $10000_b = 2^4$

* Les nombres représentés par seulement des 1 sont ceux de la forme $2^n - 1$ par exemple $1111_b = 15 = 2^4-1$

* Avec n bits (chiffres binaires) on peut compter de 0 à $2^n-1$, donc $2^n$ nombres possibles.

* Pour représenter les nombres négatifs sur un ordinateur qui utilise des nombres à $n$ bits, on partage les nombres possibles entre positifs et négatifs. Pour faciliter les calculs on utilise le **complément à deux** : $-x$ est représenté par $2^n-x$. 

    (Ainsi $x + (-x)$ vaut $x + (2^n-x) = 2^n$ ce qui vaut zéro si on travaille sur des nombres à n bits car la dernière retenue est ignorée, donc l'addition normale est compatible avec les nombres négatifs)

    Par exemple sur 3 bits on aurait ces nombres possibles :

    |binaire|signification|
    |---|---|
    |000|0|
    |001|1|
    |010|2|
    |011|3|
    |100|-4|
    |101|-3|
    |110|-2|
    |111|-1|

* Les nombres flottants (le type `float` en python) sont représentés avec un type d'écriture scientifique en binaire : par exemple $1.01101 \times 2^{17}$. Les calculs avec ces nombres peuvent être pratiques mais sont toujours sur des valeurs approchées : tester l'égalité d'un résultat avec une valeur attendue ne marchera pas toujours ! On les évite quand on peut faire avec des entiers, et sinon on teste plutôt si le résultat est suffisamment proche de la valeur attendue.

* Pour représenter du texte en binaire sur un ordinateur, on utilise un code de correspondance (chaque caractère a un code binaire fixé dans une table).
    
    * Le plus ancien et classique est [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange#Table_des_128_caract%C3%A8res_ASCII) qui représente les caractères sur 7 bits donc $2^7=128$ possibilités.  Il ne permet pas de représenter les accents du français par exemple.
    * Celui moderne maintenant utilisé presque partout est UTF-8 qui représente tous les caractères Unicode (il y en a 150000 environ !) avec une taille variable de 1 à 4 octets par caractère.