# Créer un site web avec SQL / Python / HTML / CSS et git

Page pour l'utilisation de [git](git.md)

## Étape 1 : concevoir la base de données

Utiliser une base de donnée et donc du SQL sur votre site sera utile si vous voulez :

* stocker des données créées par les personnes qui utilisent le site, ou des données sur ces personnes (par exemple des comptes, des mots de passe, des messages créés, un historique...)
* utiliser un fichier de données déjà existant (par exemple une base de donnée existante de livres, films, ...)

Il faut alors essayer de concevoir les tables SQL et leur schéma, en gardant en tête :

* les opérations que vous allez vouloir faire (par exemple, étant donné un nom d'utilisateur afficher le nombre de message posté), qui doivent être possible avec des requêtes SQL
* les principes des bases de données : pas de redondance (une info doit exister à un seul endroit), une seule valeur dans chaque case (pas de liste)

Montrez-moi ensuite ce que vous avez fait pour que je valide ou vous aide à changer.

## Étape 2 : démarrer le code

Suivez les instructions sur la page sur git si vous l'utilisez, sinon demandez-moi un lien nextcloud pour héberger votre code.

Puis vous pouvez repartir du DM de décembre donc voici [un zip avec la version complétée](corrige_sql.zip).

Faites en particulier attention :

* à la manière dont avec la bibliothèque flask on crée une fonction pour chaque page du site en utilisant `@app.route("/")` juste avant le nom de la fonction (c'est ce qu'on appelle en python un décorateur).
* aux fichiers html de _templates_ qui sont dans le dossier `templates` et aux fichiers statiques dans `static` : les premiers peuvent avoir des parties entre double accolades qui seront remplacées à chaque appel, et qui autorisent certaines fonctionnalités de python (des boucles par exemple). Pour faire le lien avec le css qui est dans static regardez comment on a utilisé `url_for` dans le html (vous pourrez juste recopier ça).
* aux formulaires dans le HTML et comment les données sont récupérées en python

Ensuite vous pourrez vous répartir le travail :

* il y a une partie HTML/CSS (éventuellement javascript) pour faire la page visible, c'est ce qu'on appelle le _frontend_.
* et une partie python/SQL pour répondre aux requêtes en renvoyer la bonne page avec les bonnes données dedans, ce qu'on appelle le _backend_.
