# Utilisation de git

??? info ":new: Installer et configurer git avec vscode sur Windows"

    Étapes :

    * installer git depuis [https://git-scm.com/download/win](https://git-scm.com/download/win) ([tuto vidéo](https://www.youtube.com/watch?v=G0UV0jKgV4Y) si besoin)
    * Ouvrir _git bash_ qui a été installé avec git et entrez les mêmes configurations initiales que sur linux :

        ```bash
        git config --global user.name "Votre nom"
        git config --global user.email "Votre email"
        ```
    * Utilisez la même command `ssh-keygen` dans git bash pour générer des clés ssh (appuyez sur Entrée pour prendre les choix par défaut), et copiez le contenu de la clé publique générée dans votre interface gitlab comme sur linux.
    * Clonez le dépôt avec `git clone` suivi de l'adresse de clonage ssh copiée depuis votre projet sur gitlab.
    * Ouvrez le dossier dans VSCode ou VSCodium (installeur [ici](https://github.com/VSCodium/vscodium/releases/download/1.72.2.22289/VSCodiumSetup-x64-1.72.2.22289.exe) si besoin), ça devrait fonctionner.

[ Introduction et création d'un projet sur gitlab](https://info.blaisepascal.fr/nsi-git/)

## Créer un projet sur gitlab, le cloner en local et l'ouvrir dans VSCodium

<iframe title="Créer un projet gitlab, cloner et ouvrir avec VSCodium" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/97779b1e-48a7-4cd7-a7a9-484fc9f10cfd" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>

Dans l'ordre la vidéo montre :

* création du projet
* création de clé ssh avec `ssh-keygen` si besoin puis copie de la clé publique `id_rsa.pub` sur gitlab
* ajout dans le fichier (le créer s'il n'existe pas) `.ssh/config` de ce texte :
    ```text
    Host gitlab.com
    Hostname altssh.gitlab.com
    Port 443
    ```
    Ceci afin de contourner le blocage du port TCP 22 (celui de SSH) au lycée. Le port 443 est celui de HTTPS, donc il n'est jamais bloqué.
* clonage du dépôt avec `git clone` et l'adresse copiée sur la page du projet
* paramétrage de git (obligatoire) :
    * `git config --global user.name "Votre nom"`
    * `git config --global user.email "Votre email"` (pas nécessairement un email valable)
    
    Ces informations seront en effet ajoutées comme signature à chacun de vos commits

* Ouverture du dossier dans VSCodium, modification du readme, commit et push (envoi du commit sur gitlab)



## Ajouter une clé ssh

Pour éviter de devoir retaper nom d'utilisateur et mot de passe à chaque fois, on peut se connecter au serveur qui contient le dépot avec le
protocole ssh en mettant sur ce serveur la clé ssh publique comme "autorisée". Se connecter est alors automatique car il suffit en déchiffrant quelque
chose de prouver qu'on possède la clé privée.

Vous pouvez vérifier si vous avez déjà des clés ssh si `id_rsa` (clé privée) et `id_rsa.pub` sont présents dans le dossier `.ssh` de votre répertoire personnel (avec la commande `ls .ssh`). Sinon, vous pouvez en générer des nouvelles avec la commande `ssh-keygen` en prenant les choix par défaut (appuyer sur Entrée à chaque fois).

Vous pouvez ensuite dans gitlab en cliquant en haut à droite puis "Preferences" puis à gauche "SSH Keys" ajouter une clé en copiant le contenu du fichier `id_rsa.pub` (affichez le avec `cat .ssh/id_rsa.pub` par exemple dans le terminal).

Si ensuite vous clonez le dépôt en utilisant l'adresse ssh (qui commence par `git@...` et PAS `https...`) alors vous n'aurez plus besoin de taper le nom d'utilisateur et mot de passe pour chaque opération.

Pour faire ceci chez vous si vous utilisez Windows, vous pouvez essayer [ceci](https://learn.microsoft.com/fr-fr/windows-server/administration/openssh/openssh_install_firstuse) puis [ceci](https://learn.microsoft.com/fr-fr/windows-server/administration/openssh/openssh_keymanagement?source=recommendations) (non testé). Alternativement vous pouvez utiliser le lien https avec identifiant/mot de passe puis une solution pour les mémoriser pour windows.

## Créer un projet et le cloner

Après avoir créé un compte gitlab, une personne dans le groupe peut créer un projet (comme expliqué dans le lien du début).

Cette personne peut ensuite ajouter les autres comptes gitlab du groupe dans le projet.

Après avoir fait l'étape précédente avec les clés ssh, vous pouvez cloner le dépôt avec la commande `git clone git@...` (l'adresse ssh est celle fournie sur la page du projet). Cela crée un dossier avec le nom du projet.

Vous pouvez ensuite ouvrir ce dossier dans VSCodium, et utiliser l'onglet "Source Control" pour les actions liées à git :

* Télécharger les modifications éventuelles avant de faire les siennes (git pull)
* Ajouter les modifications faites à enregistrer (le bouton plus pour faire git add)
* Écrire un message et faire le commit (git commit)
* Envoyer le commit sur le dépot distant (git push)

Si d'autres personnes ont effectué des modifications en même temps que vous, vous aurez à gérer une situation de fusion des modifications (git merge). C'est un peu compliqué parfois, alors essayez d'éviter au début.
