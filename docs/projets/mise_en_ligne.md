# Mettre en ligne son site créé avec flask

### 1 Se connecter sur le serveur

Dans toute la suite, il faudra remplacer **SERVEUR** et **USER** par le nom de domaine sur lequel on va mettre le site, et par le nom d'utilisateur que j'ai créé pour vous.

Vous pouvez vous connecter sur le serveur avec :

```bash
ssh USER@SERVEUR
```

Ou au lycée si le port 22 est bloqué et que le serveur écoute aussi les connections ssh sur le port 443 :

```bash
ssh -p 443 USER@SERVEUR
```

Pour copier entièrement le dossier de votre projet dans le répertoire personnel sur le serveur, on utilise `scp` qui est la version de `cp` à travers ssh, avec l'option `-r` pour copier récursivement le dossier et son contenu:

```bash
scp -P 443 -r DOSSIER_PROJET USER@SERVEUR:
```

Sur un ordinateur windows, utilisez [putty](https://www.putty.org/) pour faire une connection ssh.

### 2 Configurer le serveur web apache2

Le logiciel qui sert de serveur web est `apache2` (l'autre très utilisé est `nginx`), c'est lui qui écoute sur les ports TCP 80 et 443 pour y recevoir les requêtes web HTTP et HTTPS, et qui y répond.

Si notre site est statique, il faudrait juste lui indiquer le répertoire qui le contient pour que les fichiers (html, css, js...) soient distribués.

Pour une application flask, on va se servir d'apache comme proxy pour rediriger les requêtes vers notre programme python.

Comme il y aura plusieurs applications sur le même nom de domaine, décidez d'un nom et votre application sera hébergée à l'adresse `https://SERVEUR/nom_choisi`.

Ouvrez le fichier de configuration du site d'apache avec nano :

```bash
nano /etc/apache2/sites-available/000-default-le-ssl.conf
```

Et rajoutez juste en dessous des autres commandes `ProxyPass` :

```text
Proxypass /nom_choisi http://localhost:8002/nom_choisi
```

Puis redémarrez apache pour prendre en compte la nouvelle configuration avec :

```bash
sudo /etc/init.d/apache2 restart
```

!!! info
    Votre nom d'utilisateur a été mis dans un groupe `webdev` auquel on a donné le droit 
    
    * d'exécuter `/etc/init.d/apache2` avec sudo
    * de modifier le fichier de configuration `/etc/apache2/sites-available/000-default-le-ssl.conf`

Les requêtes vers `/nom_choisi` seront alors redirigées sur l'interface réseau `localhost` sur le port 8002 (si celui-ci est déjà pris par une autre application, prenez le suivant).

Pour tester cela, visiter avec le navigateur l'adresse `SERVEUR/nom_choisi` : vous devriez obtenir une erreur 503 "Service Unavailable" au lieu d'une erreur 404.

### 3 Lancer l'application flask

Le serveur de développement de flask ne doit pas être utilisé "en production", il faut à la place un vrai serveur WSGI, qui est le standard utilisé pour la communication entre un serveur web et un programme python. On va utiliser pour cela `gunicorn`.

Pour indiquer à gunicorn que l'application est dans le sous dossier `/nom_choisi` il faut mettre la variable d'environnement `SCRIPT_NAME` avec ce nom de sous-dossier. Cela permet de corriger les liens internes du site.

Si votre fichier python s'appelle `serveur.py`, il faudra lancer **depuis le dossier où est `serveur.py`**:

```bash
SCRIPT_NAME=/nom_choisi gunicorn -b localhost:8002 serveur:app
```

* `-b localhost:8002` sert à s'attacher (b comme bind) à l'interface réseau localhost sur le port 8002 (à changer si vous en avez mis un autre dans la configuration d'apache).
* `serveur:app` signifie que l'application Flask est dans la variable `app` dans le fichier `serveur.py`

Naviguez à nouveau vers l'adresse `SERVEUR/nom_choisi`, en principe ça devrait marcher !

Si vous voulez vous déconnecter de la connection ssh sans arrêter gunicorn, vous pouvez faire :

* **Ctrl+Z** pour suspendre le processus et récupérer le terminal
* `bg` pour passer le processus suspendu en arrière plan (**b**ack**g**round)
* Ensuite **Ctrl+D** termine la connection ssh
* _Si vous vous reconnectez, il faudra arrêter gunicorn avec `pkill gunicorn` avant de pouvoir le relancer_

!!! bug "Erreurs possibles"
    Si vous avez dans votre code à la fin d'une fonction quelque chose comme `return redirect("/")` cela ne prendra pas en compte le préfixe `/nom_choisi`.

    Pour corriger cela il faut utiliser la fonction `url_for` en lui donnant le nom de la fonction python qui gère la page vers laquelle on redirige, par exemple : `return redirect(url_for("page_principale"))`, après l'avoir importée avec `from flask import url_for`.

!!! info "Mettre à jour après une modification"
    Si vous avez modifié un peu votre code en local, vous pouvez le supprimer sur le serveur puis ré-envoyer la nouvelle version, mais c'est dommage de tout renvoyer à chaque fois. 
    
    La commande `rsync` permet de synchroniser les changements en envoyant seulement ce qui a changé. Par exemple avec :

    ```bash
    rsync -a -v -h --del DOSSIER_PROJET USER@SERVEUR:DOSSIER_PROJET
    ```

    on va synchroniser en mode archive (-a) avec un affichage verbeux et lisible (-v et -h) en supprimant sur le serveur les fichiers supprimés en local (--del).