# DM : écrire la partie SQL d'un serveur en python

Les schémas des tables sont : (* pour les clés primaires, # pour les clés étrangères)

user(name* TEXT, password TEXT)
discussion(id* INTEGER, sujet TEXT)
message(id* INTEGER, auteur# TEXT, discussion_id# INTEGER, contenu TEXT)

Le serveur est déjà écrit entièrement dans le fichier serveur.py

Vous pouvez le lancer et aller voir le résultat avec le lien qui s'affiche dans la console (possible depuis tous les appareils qui ont accès au même réseau).
Si flask n'est pas installé, dans la console tapez : "pip install flask"

Dans le serveur.py, il manque juste toutes les requêtes SQL.

Faites les TODO dans l'ordre à partir de TODO 1, en testant à chaque fois sur la page web créée.

Vous pouvez aussi améliorer l'apparence et le contenu du site en modifiant les fichiers html dans le dossier templates ou le fichier css dans le dossier static.

RENDU: Envoyez-moi le fichier serveur.py complété avec vos requêtes en pièce jointe sur la messagerie ENT.

BONUS : la sécurité du site est très mal gérée avec les cookies : pouvez-vous vous connecter avec un autre nom d'utilisateur ? 
(ce n'est pas une injection SQL, sauf si vous avez mal écrit le SQL)