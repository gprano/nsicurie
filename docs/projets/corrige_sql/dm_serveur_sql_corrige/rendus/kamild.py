import re
from flask import Flask, make_response, redirect, render_template, request
import sqlite3


def creation_base():
    with sqlite3.connect("database.db") as con:
        cur = con.cursor()
        cur.executescript(
            """
        CREATE TABLE IF NOT EXISTS user(name TEXT PRIMARY KEY, password TEXT);
        CREATE TABLE IF NOT EXISTS discussion(id INTEGER PRIMARY KEY AUTOINCREMENT, sujet TEXT);
        CREATE TABLE IF NOT EXISTS message(id INTEGER PRIMARY KEY AUTOINCREMENT,
                                           auteur TEXT,
                                           discussion_id INTEGER,
                                           contenu TEXT,
                                           FOREIGN KEY (auteur) REFERENCES user(name),
                                           FOREIGN KEY (discussion_id) REFERENCES discussion(id));
        """
        )


app = Flask(__name__)

def executer_requete(requete, remplacements_points_interrogation):
    """Fonction pour se connecter à la base, effectuer une requete et renvoyer tous les résultats
    prend en paramètre la requête et la liste des valeurs qui vont remplacer les "?" de la requête
    renvoie la liste des tuples des lignes de résultat
    """
    with sqlite3.connect("database.db") as connection:
        curseur = connection.cursor()
        curseur.execute("PRAGMA foreign_keys = ON")
        curseur.execute(requete, remplacements_points_interrogation)
        return curseur.fetchall()

@app.route("/discussion/<int:discussion_id>")
def affiche_discussion(discussion_id):
    # Exemple de requête écrite complétement (rien à modifier)
    requete_titre = "SELECT sujet FROM discussion WHERE id = ?"
    titre = executer_requete(requete_titre, [discussion_id])[0][0]
    # TODO 6 Ajoutez une requête pour obtenir les attributs auteur et contenu de la table message
    # SI le discussion_id est bien égal à la variable discussion_id (à remplacer par ?) !
    requete = "SELECT auteur, contenu FROM message WHERE discussion_id = ?"
    messages = executer_requete(requete, [discussion_id]) # complétez la liste
    return render_template("discussion.html", titre=titre, messages=messages)


@app.route("/discussion/<int:discussion_id>", methods=["POST"])
def ajout_message(discussion_id):
    user = request.cookies.get("nom", None)
    if user == None:
        return "Vous devez être connecté pour poster"
    message = request.form["message"]
    # TODO 5 Ajoutez le message dans la table message
    # l'id sera NULL (autoincrement donc ajouté automatiquement par sqlite)
    # pour le reste vous aurez besoin de user, discussion_id et message (mettre des ? et remplacer)
    requete = "INSERT INTO message VALUES (NULL, ?, ?, ?)"
    executer_requete(requete, [user,discussion_id,message]) # complétez la liste
    return affiche_discussion(discussion_id)


@app.route("/creer_utilisateur", methods=["POST"])
def creer_utilisateur():
    nom = request.form["nom"]
    motdepasse = request.form["motdepasse"]
    # TODO 4 écrivez la requête pour ajouter un utilisateur, et mettez les variables
    # dans la liste pour remplacer les ?
    requete = "INSERT INTO user VALUES (?, ?)"
    executer_requete(requete, [nom,motdepasse]) # complétez la liste
    return redirect("/")


@app.route("/login", methods=["POST"])
def login():
    nom = request.form["nom"]
    motdepasse = request.form["motdepasse"]
    # TODO 3 écrire une requete de connexion qui sélectionne tout (*) dans la table user
    # si l'attribut name est égal à la variable nom, et l'attribut password est égal à la variable motdepasse
    # mettez des ? à la place des variables, et remplacez les valeurs dans la liste dans l'appel à executer_requete
    requete = "SELECT * FROM user WHERE name = ? AND password = ?"
    resultat = executer_requete(requete, [nom,motdepasse]) #ajoutez les valeurs nom,motdepasse dans la liste (dans le bon ordre de votre requête)
    if len(resultat) > 0:
        resp = make_response(redirect("/"))
        resp.set_cookie("nom", nom)
        return resp
    return redirect("/")


@app.route("/")
def page_principale():
    nom = request.cookies.get("nom", None)
    # TODO 1 écrire une requête qui renvoie tous les sujet et id de la table discussion
    requete = "SELECT * FROM discussion"
    discussions = executer_requete(requete, [])
    return render_template("index.html", discussions=discussions, nom=nom)


@app.route("/", methods=["POST"])
def ajout_discussion():
    nouveau_titre = request.form["nouveau_titre"]
    # TODO 2 Écrire une requête qui ajoute une discussion dans la table
    # On pourra mettre NULL comme id car cette valeur est déclarée comme AUTOINCREMENT
    # Donc sqlite mettra une valeur pas encore utilisée directement
    # le titre est récupéré dans la variable nouveau_titre depuis le formulaire
    # mettez un ? dans la requête à sa place, la valeur sera remplacée grâce au deuxième paramètre
    # de la fonction executer_requete
    requete = "INSERT INTO discussion VALUES (NULL, ?)"
    executer_requete(requete, [nouveau_titre])
    return page_principale()


if __name__ == "__main__":
    creation_base()
    app.run("0.0.0.0", 8080)
