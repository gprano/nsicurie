import re
from flask import Flask, make_response, redirect, render_template, request
import sqlite3

from werkzeug.datastructures import D


def creation_base():
    with sqlite3.connect("database.db") as con:
        cur = con.cursor()
        cur.executescript(
            """
        CREATE TABLE IF NOT EXISTS user(name TEXT PRIMARY KEY, password TEXT);
        CREATE TABLE IF NOT EXISTS discussion(id INTEGER PRIMARY KEY AUTOINCREMENT, sujet TEXT);
        CREATE TABLE IF NOT EXISTS message(id INTEGER PRIMARY KEY AUTOINCREMENT,
                                           auteur TEXT,
                                           discussion_id INTEGER,
                                           contenu TEXT,
                                           FOREIGN KEY (auteur) REFERENCES user(name),
                                           FOREIGN KEY (discussion_id) REFERENCES discussion(id));
        """
        )


app = Flask(__name__)

def executer_requete(requete, remplacements_points_interrogation):
    """Fonction pour se connecter à la base, effectuer une requete et renvoyer tous les résultats
    prend en paramètre la requête et la liste des valeurs qui vont remplacer les "?" de la requête
    renvoie la liste des tuples des lignes de résultat
    """
    with sqlite3.connect("database.db") as connection:
        curseur = connection.cursor()
        curseur.execute("PRAGMA foreign_keys = ON")
        curseur.execute(requete, remplacements_points_interrogation)
        return curseur.fetchall()

@app.route("/discussion/<int:discussion_id>")
def affiche_discussion(discussion_id):
    # Exemple de requête écrite complétement (rien à modifier)
    requete_titre = "SELECT sujet FROM discussion WHERE id = ?"
    titre = executer_requete(requete_titre, [discussion_id])[0][0]
    requete = "SELECT auteur, contenu FROM message WHERE discussion_id = ?"
    messages = executer_requete(requete, [discussion_id]) 
    return render_template("discussion.html", titre=titre, messages=messages)


@app.route("/discussion/<int:discussion_id>", methods=["POST"])
def ajout_message(discussion_id):
    user = request.cookies.get("nom", None)
    if user == None:
        return "Vous devez être connecté pour poster"
    message = request.form["message"]
    requete = "INSERT INTO message(auteur, discussion_id,contenu) VALUES(?,?,?)"
    executer_requete(requete, [user,discussion_id,message])
    return affiche_discussion(discussion_id)


@app.route("/creer_utilisateur", methods=["POST"])
def creer_utilisateur():
    nom = request.form["nom"]
    motdepasse = request.form["motdepasse"]
    requete = "INSERT INTO user(name, password) VALUES(?,?)"
    executer_requete(requete, [nom, motdepasse]) 
    return redirect("/")


@app.route("/login", methods=["POST"])
def login():
    nom = request.form["nom"]
    motdepasse = request.form["motdepasse"]
    
    requete = "SELECT * FROM user WHERE name = ? AND password = ?"
    resultat = executer_requete(requete, [nom, motdepasse]) 
    resp = make_response(redirect("/"))
    resp.set_cookie("nom", nom)
    return resp
    return redirect("/")


@app.route("/")
def page_principale():
    nom = request.cookies.get("nom", None)
    requete = "SELECT id,sujet FROM discussion"
    discussions = executer_requete(requete, [])
    return render_template("index.html", discussions=discussions, nom=nom)


@app.route("/", methods=["POST"])
def ajout_discussion():
    nouveau_titre = request.form["nouveau_titre"]
    
    requete = "INSERT INTO discussion(sujet) VALUES(?)"
    executer_requete(requete, [nouveau_titre])
    return page_principale()


if __name__ == "__main__":
    creation_base()
    app.run("0.0.0.0", 8080)