# Le langage SQL (Structured Query Language -> Langage de Requêtes Structurées)

Un principe des bases de données est de séparer :

* La représentation des données, sous forme de tables selon le **modèle relationnel**.
* La manière pratique dont ces données sont stockées et manipulées en mémoire, donc
les structures de données. Celles-ci peuvent être complexes (B-tree, ...), réparties
sur plusieurs machines, permettrent à plusieurs logiciels d'utiliser la base en même temps, etc. 
C'est le travail d'un **logiciel de gestion de base de données** (SGBD) et on n'en verra pas les détails cette année.

Pour utiliser nos données, on va donc pas les manipuler directement mais on va envoyer
des **requêtes** au SGBD. Le langage principalement utilisé pour ces requêtes est **SQL**.

Les requêtes SQL sont écrites sur une ou plusieurs lignes, terminées par ';'. La casse (majuscule/minuscule)
est ignorée par le langage mais par convention on écrit les mots-clés en MAJUSCULES et le reste
en minuscule.

## 1 Création de Table

Prenons le diagramme suivant pour une base de données de livres :

![schéma](../images/schema_livre_auteur.png)

Le schéma est le suivant:

Livre(<u>titre</u> STRING, ann_publi INT, id_auteur INT)

Auteur(<u>id</u> INT, nom STRING, prenom STRING, ann_nai INT)

Et en SQL on peut créer les tables avec les requêtes :

```sql
CREATE TABLE Auteur(id INT PRIMARY KEY,
                    nom TEXT,
                    prenom TEXT,
                    ann_nai INT);

CREATE TABLE Livre(titre TEXT PRIMARY KEY,
                   ann_publi INT,
                   id_auteur INT,
                   FOREIGN KEY (id_auteur) REFERENCES Auteur(id));
```

## 2 Recherche dans une table

Les requêtes commencent toujours par **SELECT** et ne modifient pas les tables.

Le résultat de la requête est lui-même une table.

### Obtenir toute la table

```sql
SELECT * FROM nom_de_la_table;
```

### Ne garder que certaines colonnes (ou attributs)

```sql
SELECT attribut1 FROM nom_de_la_table;

SELECT attribut1, attribut2 FROM nom_de_la_table;
```

### Renommer les colonnes du résultat avec **AS**

```sql
SELECT attribut1 as nouveau_nom1, attribut2 as nouveau_nom2 FROM nom_de_la_table;
```

### Ne garder que certaines lignes (ou entités, ou enregistrements, ou n-uplets) avec **WHERE**

```sql
SELECT * FROM nom_de_la_table WHERE condition;
```

`condition` peut servir à ne garder que les lignes où :

* un attribut a une certaine valeur, ou satisfait une comparaison avec une valeur ou un autre attribut :
    - `nom_attribut = 10`
	- `nom_attribut > 0`
	- `nom_attribut1 = nom_attribut2`
* si l'attribut est une chaîne de caractères, on peut la comparer à un
  motif avec **LIKE** en utilisant `%` pour remplacer n'importe quelle
  chaîne et `_` pour remplacer n'importe quel caractère.
    - `titre LIKE '%Terre%'` garde les lignes où l'attribut titre contient le mot Terre.
	- `prenom LIKE 'A%'` garde les lignes où l'attribut prenom commence par A.
	- `code_postal LIKE '38___'` garde les lignes où le code postal est de la forme 38???.
* on peut combiner des condition avec **AND**, **OR** et **NOT**
    - `age > 70 AND age <= 75`
	- `moyenne_bac >= 10 OR (moyenne_bac >= 8 AND rattrapage_reussi = True)`
	
### Ne pas garder les doublons avec **DISTINCT**

Si on ne garde que certaines colonnes, on peut avoir plusieurs lignes identiques
dans le résultat. On peut les enlever en remplaçant `SELECT ...` par `SELECT DISTINCT ...`.

### Trier les résultats avec **ORDER BY**

On peut rajouter à la fin de la requête `ORDER BY attribut ASC` ou
`ORDER BY attribut DESC` qui trie les résultats par ordre croissant
ou décroissant de la valeur dans la colonne `attribut`.

Si on écrit `ORDER BY attribut1 ASC, attribut2 ASC`, les résultats
seront triés par attribut1, et à valeur égale par l'attribut2.

### Utiliser des fonctions d'aggrégation

On peut utiliser les fonctions **COUNT**, **SUM**, **AVG**, **MAX** et **MIN**
quand on sélectionne un attribut pour :

* compter le nombre de lignes de la réponse
  ```sql
  SELECT COUNT(*) FROM table WHERE age=17
  ```
* additionner toutes les valeurs de la colonne
  ```sql
  SELECT SUM(prix) FROM commandes
  ```
* faire la moyenne d'une colonne
  ```sql
  SELECT AVG(note) FROM bulletins
  ```
* trouver la valeur maximale ou minimale d'une colonne
  ```sql
  SELECT MIN(age) FROM participants
  ```
  
À chaque fois, la réponse est un tableau avec **une seule case**.

### Tout ça à la fois

Bien sûr on peut un peu tout combiner:

* Calculer la moyenne des élèves dont le prénom commence par un A :
  ```sql
  SELECT AVG(note) AS moyenne FROM eleves WHERE prenom LIKE 'A%'`
  ```
  
### Requêtes imbriquées

On peut mettre des requêtes dans des requêtes :

* Pour faire une recherche sur une table créée par une autre recherche :
  ```sql
  SELECT ... FROM (requête qui renvoie une table)
  ```
* Dans une clause WHERE, une requête qui renvoie une seule case peut être utilisée comme valeur :
  ```sql
  SELECT nom,prenom FROM ma_table WHERE note = (SELECT MAX(note) FROM ma_table)
  ```
  Cette requête renvoie les noms et prénoms des personnes ayant obtenue la note la plus haute.
