# Correction du DM de SQL (serveur de discussion)

Le [sujet](../dm_serveur_sql.zip) au format zip à télécharger et extraire.

### TODO 1

```python linenums="93"
    requete = "SELECT sujet,id FROM discussion"
    discussions = executer_requete(requete, [])
```

!!! danger "Erreur fréquente observée"
    L'ordre était important car le fichier `templates/index.html` affichait la première valeur
    comme titre. Si vous avez vu seulement "1" et "2" à la place des titres de discussions, c'est
    sans doute parce que vous avez écrit `SELECT id,sujet` ou bien `SELECT *`

### TODO 2

```python linenums="107"
    requete = "INSERT INTO discussion VALUES (NULL, ?)"
    executer_requete(requete, [nouveau_titre])
```

On commence à utiliser la fonctionnalité de SQLite de remplacer les variables dans les requêtes
par des `?` puis les donner à part pour éviter les failles de sécurité de type _injection SQL_.

Ceci **ne fait pas partie du langage SQL**, mais des fonctionnalités proposées par les systèmes de gestion
de base de donnée pour protéger contre une modification malintentionnée de la requête.

(Il ne faut donc pas dans un sujet de type bac écrire des requêtes SQL avec des `?` dedans...)

### TODO 3

```python linenums="80"
    requete = "SELECT * FROM user WHERE name=? AND password=?"
    resultat = executer_requete(requete, [nom,motdepasse])
```

Si on donne un couple (nom, mot de passe) qui n'est pas dans la table user, cette requête
ne renverra rien.

### TODO 4

```python linenums="68"
    requete = "INSERT INTO user VALUES (?, ?)"
    executer_requete(requete, [nom, motdepasse])
```

### TODO 5

```python linenums="57"
    requete = "INSERT INTO message VALUES (NULL, ?, ?, ?)"
    executer_requete(requete, [user, discussion_id, message])
```

### TODO 6

```python linenums="43"
    requete = "SELECT auteur, contenu FROM message WHERE discussion_id=?"
    messages = executer_requete(requete, [discussion_id])
```

On remarque que les messages ne sont pas stockés dans des tables différentes
pour chaque discussion. À la place, on filtre la table message pour obtenir
seulement les messages de la discussion à afficher (ceux qui ont le bon `discussion_id`).

### BONUS

Comment tricher à la connection ?

Vous pouvez re-regarder le code : comme dans beaucoup de sites, pour éviter de redemander le
mot de passe à l'utilisatrice à chaque requête, on stocke un cookie sur son navigateur. Ce
cookie est ré-envoyé à chaque fois et prouve qu'il s'agit bien de la bonne personne.

Mais ici, au lieu de stocker une valeur aléatoire et impossible à deviner, on stocke juste un
cookie dont la clé est "nom" et la valeur... le nom de l'utilisatrice.

Il suffit donc de taper F12 pour ouvrir la console de développement, d'aller dans l'onglet stockage (pour Firefox)
et de créer un cookie qui s'appelle "nom" et qui a comme valeur "admin" pour être connecté comme admin.

![](cookie.png)

!!! info "Cookies de session"
    Sur les sites où vous êtes connectés, vous pouvez essayer d'aller chercher le cookie de session, et copier sa
    valeur dans un autre navigateur ou sur un autre ordinateur. En général, vous serez automatiquement
    connecté sans avoir à retaper le mot de passe (sauf si le site se rend compte d'une autre manière que ce n'est pas
    la même connection).