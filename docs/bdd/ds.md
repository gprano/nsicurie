# Devoir Base de données & Cryptographie

[Sujet](ds_sql_crypto.pdf) du devoir.

[Corrigé](corrige_sql_crypto.pdf).

Et pour conclure, [un lien](https://dustri.org/b/horrible-edge-cases-to-consider-when-dealing-with-music.html)
vers la liste amusante de tous les exemples bizarres à prendre en compte si on veut créer une base de donnée
de musique. 

Et le [schéma](https://wiki.musicbrainz.org/images/5/52/ngs.png) qui essaye de gérer tout ça.
