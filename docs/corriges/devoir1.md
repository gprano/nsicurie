# Corrigé du premier devoir

[Sujet](../pdf/devoir1.pdf)

---

## Exercice 1
_Programmation python, algorithmes récursifs, diviser pour régner_

### Questions préliminaires

**1.** (1,3) est une inversion car 1<3 et tab[1] = 8 > tab[3] = 7.

**2.** (2,3) n'est pas une inversion car tab[2] = 3 < tab[3] = 7.

### Partie A

**1.a**

* `fonction1([1,5,3,7],0)` renvoie 0
* `fonction1([1,5,3,7],1)` renvoie 1
* `fonction1([1,5,2,6,4],1)` renvoie 2

**1.b** `fonction1(tab,i)` calcule le nombre d'inversions dont i est le premier élément.

**2.**

```python
def nombre_inversion(tab):
	n = len(tab)
	cpt = 0
	for i in range(n):
		cpt = cpt + fonction1(tab,i)
	return cpt
```

**3.** La complexité de l'algorithme est quadratique (car la fonction1 a une complexité linéaire et
est appelée elle-même n fois).

### Partie B

**1.** Le tri fusion a une complexité meilleure que quadratique (quasi-linéaire).

**2.**

```python
def moitie_gauche(tab):
	n = len(tab)
	taille_gauche = n // 2
	if n % 2 == 1:
		taille_gauche += 1
	tab_gauche = []
	for i in range(taille_gauche):
		tab_gauche.append(tab[i])
	return tab_gauche
```
On pourrait aussi directement écrire `taille_gauche = (n+1)//2` ce qui donne l'arrondi au dessus de la division de n par 2.

**3.**
Il faut vraiment suivre les étapes du sujet, sans oublier de gérer le cas de base pour éviter une récursion infinie.

```python
def nb_inversions_rec(tab):
	n = len(tab)
	# cas de base
	if n <= 1:
		return 0
	# séparer le tableau en deux
	tab_g = moitie_gauche(tab)
	tab_d = moitie_droite(tab)
	# appels récursifs
	resultat = 0
	resultat += nb_inversions_rec(tab_g) 
	resultat += nb_inversions_rec(tab_d)
	# tri
	tab_g = tri(tab_g)
	tab_d = tri(tab_d)
	# ajout des inversions entre les deux
	resultat += nb_inv_tab(tab_g,tab_d)
	return resultat
```

---

## Exercice 2

### Partie A

**1.** `ps` permet d'obtenir l'état des processus (similaire à `top` mais sans être en temps réel).

**2.** L'identifiant d'un processus est son **PID**.

**3.** Le partage du processeur entre les processus est l'**ordonnancement**.

**4.** La commande `kill` permet d'interrompre un processus sur linux.

### Partie B

**1.** À chaque instant, on exécute le processus de numéro de priorité le plus bas parmi ceux qui
sont déjà arrivés et pas encore exécutés entièrement.

Cela donne (avec une `*` quand un processus se termine et un `'` quand il vient d'arriver):
```
|P3'|P3|P2'|P1'|P1|P1*|P2|P2*|P3|P3*|
```

**2.** Le scénario 2 provoque un interblocage, car :

* P1 possède R1 et attend R2
* P3 possède R2 et attend R1

Les deux processus se bloquent donc mutuellement.

### Partie C

**1.a.**

| binaire | 0110 | 0011 | 0100 | 0110 |
|---|---|---|---|---|
| decimal | 4+2=6 | 2+1=3 | 4 | 4+2=6 |
| hexadécimal | 6 | 3 | 4 | 6 |
| caractères | 63 -> c | | 46 -> F |

Donc le message est **cF**.

Remarque : les encodages en hexadécimal sont les mêmes qu'en décimal ici car aucun groupe de 4 bits n'a une valeur qui dépasse 9. Si on avait par exemple 1101, ça donnerait 13 en décimal et D en hexadécimal.

**1.b.**

| message | 0110 | 0011 | 0100 | 0110 |
|---|---|---|---|---|
| clé | 1110 | 1110 | 1111 | 0000 |
| message XOR clé | 1000 | 1101 | 1011 | 0110 |

**2.a.**

| a | b | a XOR b | (a XOR b) XOR b |
| --- | --- | --- | --- |
| 0 | 0 | 0 | 0 |
| 0 | 1 | 1 | 0 |
| 1 | 0 | 1 | 1 | 
| 1 | 1 | 0 | 1 |


**2.b** On remarque que la colonne a est la même que la colonne (a XOR b) XOR b,
c'est à dire que (a XOR b) XOR b = a.

Donc faire le XOR deux fois par la même clé redonne le message initial. Pour déchiffrer un message qui a été chiffré par XOR avec une clé, si on dispose de la clé, il suffit de refaire un XOR entre le message chiffré et la clé.
