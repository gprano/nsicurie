# Aides diverses pour le projet

!!! info "Installer chez soi python, vscodium, pgzero"
    Vous avez besoin d'installer chez vous :

    * python3 depuis le [site de python](https://www.python.org/)


        !!! warning "Bien cocher la case "Ajouter python au PATH" à l'installation"

            Si vous ne l'avez pas fait, on peut chercher comment le faire à la main mais le plus facile sera sans doute de désinstaller et ré-installer python.

            C'est cela qui permet d'utiliser les commandes `python` et `pip` depuis le terminal (cmd).

    * [vscodium](https://github.com/VSCodium/vscodium). 
    
        Les exécutables sont disponibles [ici](https://github.com/VSCodium/vscodium/releases).

        Vous aurez à priori besoin du fichier [ VSCodiumSetup-x64-1.72.2.22289.exe ](https://github.com/VSCodium/vscodium/releases/download/1.72.2.22289/VSCodiumSetup-x64-1.72.2.22289.exe) si votre ordinateur a l'architecture x86-64.
    
        Si vous n'y arrivez vraiment pas il est aussi possible de faire avec VScode (qui envoie des informations de télémétrie à Microsoft).
    * Les extensions **ms-python** et **french language pack** au moins (pyright en option si vous voulez la vérification des types).

    * et pygame zero, via le gestionnaire de bibliothèques python pip. 
    
        Ouvrez une ligne de commande ([aide](https://www.lifewire.com/how-to-open-command-prompt-2618089)) et tapez `pip install pgzero` 

        !!! warning "En cas d'erreur à l'installation"
            * Si pip n'est pas installé => ré-installez python en cochant la case pour le mettre dans le PATH.

            * Si pip essaie de télécharger un fichier .tar.gz pour pygame puis a une erreur, c'est qu'il essaie de recompiler pygame depuis le code source,
            ce dont on n'a a priori pas envie. Pour demander à télécharger un paquet pré-compilé vous pouvez utiliser la commande `pip install --only-binary=all pgzero`
            qui devrait cette fois télécharger un fichier .whl (le format "wheel" des paquets pré-compilés python).

        !!! warning "En cas d'erreur à l'exécution à cause de l'import de screen depuis pgzero.builtins"
            Vous pouvez juste ne pas importer screen (si vous avez pyright il soulignera screen en rouge mais ça ne causera pas d'erreur à l'installation, au lycée j'ai installé la dernière version de pgzero depuis github pour pouvoir importer screen sans erreur)

??? warning "Exécuter avec pgzrun"
    Pour éviter un bug qu'on a eu au début, il convient de lancer votre programme non pas avec `python3 ...` mais avec `pgzrun nomdevotrfichier.py`, que vous pouvez taper directement dans le terminal en bas de VSCodium.

    Il n'y a alors pas besoin de `import pgzrun` ni de `pgzrun.go()`.

    Si vous voulez éviter les soulignement en rouge de l'extension Pyright qui ne connaît pas les objets de pygame zero, vous pouvez importer ceux que vous utilisez comme ceci :
    `from pgzero.builtins import Actor,keyboard`

??? info "Les classes Rect et Actor"
    La classe Rect permet de définir une zone rectangulaire de l'écran, par exemple un bouton dans lequel on détecte un clic, une zone de jeu...

    Voir par exemple la manière dont sont gérés les boutons pour changer de couleur dans ce [TP](POO/pile_undo.md)

    La classe Actor permet de définir un élément avec une image, voir [la documentation](https://pgzero-french.readthedocs.io/fr/latest/builtins.html#acteurs).

    En particulier, les deux classes définissent les méthodes :

    * collidepoint((x,y)) -> bool
    * colliderect(Rect) -> bool

    Qui permettent de tester si un point est dans le rectangle (par exemple le clic de la souris), et si deux rectangles/acteurs sont en collision.

    Vous pouvez utiliser des objets de type Rect ou Actor comme attributs de vos propres objets pour gérer plus facilement les positions et collisions (on peut également s'en passer et le faire à la main).

??? hint "Gestion des délais et du temps"
    Il faut éviter d'utiliser la fonction sleep du module time pour ajouter des délais entre les actions : celle-ci va bloquer le programme en entier, donc les fonctions draw et update ne seront plus appelées pendant ce temps là.

    À la place, on peut utiliser le module clock de pgzero qui permet de planifier l'appel d'une fonction une fois dans le futur ou bien à intervalles régulier : voir la [documentation](https://pgzero-french.readthedocs.io/fr/latest/builtins.html#horloge)

    Si on veut tenir compte du temps dans le jeu sans que cela dépende de la vitesse de l'ordinateur, on peut ajouter un paramètre `dt` à la fonction update qui donnera en secondes le temps écoulé depuis le dernier appel. Petit exemple :

    ```python
    timer = 0
    def update(dt):
        global timer
        timer += dt
        if timer > 10:
            print("10s sont écoulées !")
    ```

??? hint "Gérer la gravité"
    **Rappel de physique :** sur Terre, l'accélération due à la gravité est constante, vers le bas (c'est la constante **g**).
    Cela veut dire que la vitesse vers le bas d'un objet qui tombe augmente de manière régulière. Si on ne tient pas compte des frottements de l'air, la chute ne dépend pas de la masse de l'objet.

    Pour simuler la gravité d'un objet qui tombe, il suffit donc à chaque tour du jeu d'augmenter la vitesse verticale vers le bas d'une constante. Vous pouvez faire des essais, dans tous les cas cette constante devrait être assez basse (0.1 ou 0.5 par exemple) car la fonction update est exécutée très souvent.

    Bien sûr, il faudra aussi gérer ce qu'il se passe pour que la chute s'arrête.

    Pour simuler un saut vers le haut, il suffit de remettre la vitesse verticale à une valeur constante vers le haut.