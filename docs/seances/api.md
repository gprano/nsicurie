# Utilisation d'API web

Une **API** (Application Programming Interface) est un ensemble de fonctions qu'on peut utiliser depuis un programme, sans avoir besoin de savoir comment le service fonctionne à l'intérieur. On distingue :

* les API de bibliothèques de code (comme les modules pygame ou pillow qu'on a utilisés)
* les API web où les appels se font par des requêtes HTTP à un serveur sur internet

On va voir quelques exemples d'API web :

### Position de la station spatiale internationale

Allez visiter la page [http://api.open-notify.org/iss-now.json](http://api.open-notify.org/iss-now.json). Ce format de données s'appelle JSON, il est à la fois lisible par les humains et facile à traiter automatiquement avec un programme. À quel type en python est-ce que cela ressemble ?

Pour obtenir le résultat en python on utilise le module `requests` :

```python
import requests
r = requests.get("http://api.open-notify.org/iss-now.json").json()
```

Vérifiez le type de la valeur qu'on obtient dans la variable `r`.

Ajoutez le code pour récupérer la latitude et la longitude de l'ISS dans deux variables `lat` et `lon`.

### Intégration dans une page web avec flask

On peut maintenant faire une page web qui affichera la position de l'ISS à chaque fois qu'on la visite. Créez un nouveau fichier avec ce code :

```python
from flask import Flask, render_template
import requests

app = Flask(__name__)

@app.route("/")
def page():
    # ajoutez votre code précédent pour faire la requête et créer les variables lat et lon
    return render_template('space.html',lat=lat,lon=lon)

if __name__ == "__main__":
    app.run('0.0.0.0', 8080)
```

et dans un sous-dossier `templates` créer le fichier `space.html` suivant :

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>ISS</title>
  </head>
  <body>
    <h1>Position de la Station Spatiale Internationale</h1>
    <p>L'iss est actuellement à {{ lat }} de latitude et {{ lon }} de longitude</p>
  </body>
</html>
```

Et testez-le.

### Ajouter la météo

Beaucoup d'API web nécessitent une **clé** pour y avoir accès, gratuite ou pas selon les API, et qu'on renvoie comme paramètre à chaque requête pour s'identifier.

Openweathermap est une API qui permet d'obtenir la météo à un endroit donné, on peut faire deux types de requêtes :

* `https://api.openweathermap.org/data/2.5/weather?q=LOCATION&units=metric&appid=KEY`
    pour obtenir la météo avec une recherche d'endroit (nom de ville par exemple)
* `https://api.openweathermap.org/data/2.5/weather?lat=LATITUDE&lon=LONGITUDE&appid=KEY`
    pour obtenir la météo à partir des coordonnées GPS

!!! info "rappels de HTTP"
    Observez, si vous ne le saviez plus, la manière dont des paramètres peuvent être passés dans l'URL avec une requête GET : `?clé1=valeur1&clé2=valeur2`...

    On avait vu l'an dernier que les requêtes POST ne passaient pas les paramètres dans l'URL et servaient plutôt pour les messages qui faisaient une action sur le serveur (poster un message, commander sur un site de vente, se connecter avec un mot de passe...).

À l'aide de la clé que je vous donnerai, faites une requête depuis python pour obtenir le résultat de la météo dans une grande ville de votre choix.

!!! info "f-strings"
    Pour remplacer LOCATION, KEY et autres paramètres dans l'URL en python il est pratique d'utiliser les f-string :

    * on écrit f devant les guillemets `f"test"`
    * on met entre accolades les variables dont on veut afficher la valeur à cet endroit de la chaîne de caractère : `f"la variable i vaut {i}"`
    * python évalue cette f-string à chaque fois et la remplace par la chaîne de caractère normale qu'il obtient (le résultat est toujours de type str).

Puis modifiez le site qui affiche les coordonées de la station spatiale internationale pour afficher également des informations de la météo de là où elle se trouve.

## Fin de TP au choix (mini-projet de vacances)

En vous inspirant du code du DM de SQL des vacances de noël, vous pouvez :

* ajouter un formulaire pour que l'utilisateur choisisse la ville dont il veut avoir la météo
* stocker dans un cookie la ville de l'utilisateur pour s'en souvenir à sa prochaine visite
* ou, stocker dans une base de donnée l'association (ip de la visite) -> (ville) pour s'en souvenir la prochaine fois que cette ip fait une requête
* voire, créer un mot de passe qui permet d'enregistrer une liste de villes et quand on le tape on obtient un résumé de la météo sur toutes ces villes ?

Ou bien, voici une [liste d'API gratuites à utiliser](https://github.com/public-apis/public-apis) (celles sans authentification seront plus simples), à vous de faire une petite page qui en utilise une ou plusieurs de votre choix.